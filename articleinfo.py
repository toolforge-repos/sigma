#!/home/sigma/.local/bin/python3
# -*- coding: utf-8 -*-
# Copyright (c) 2015 User:Σ, GPLv3

import collections
from datetime import timedelta
import statistics
import asyncio

import aiomysql as sql
from arrow import Arrow

from utils import Databank, connect_info, format_seconds
from tool import Tool


class DictishThing:
    __slots__ = ()
    def __repr__(self):
        d = {}
        for k in self.__slots__:
            d[k] = getattr(self, k)
        return "{0}({1!r})".format(type(self).__name__, d)

    def __eq__(self, other):
        return repr(self) == repr(other)


class MonthCounter(DictishThing):
    __slots__ = 'all', 'minor', 'anon', 'size'
    def __init__(self):
        self.all = self.minor = self.anon = 0
        self.size = []  # array??? fucking php


class YearCounter(DictishThing):
    __slots__ = 'all', 'minor', 'anon', 'months'
    def __init__(self):
        self.all = 0
        self.minor = 0
        self.anon = 0
        # 0 vs 1 indexing crap
        self.months = [None]
        self.months.extend(MonthCounter() for _ in range(12))


class CountCounter(DictishThing):
    __slots__ = 'today', 'week', 'month', 'year'
    def __init__(self):
        self.today = self.week = self.month = self.year = 0


class UserContainer(DictishThing):
    __slots__ = 'all', 'minor', 'first', 'last', 'atbe', 'minorpct', 'size', 'addsize', 'remsize', 'deltasize'
    def __init__(self):
        self.all = self.minor = 0
        self.first = None
        self.last = None
        self.atbe = None
        self.minorpct = 0
        self.size = []  # array??? fuck php
        self.addsize = 0
        self.remsize = 0
        self.deltasize = []


class ArticleInfo(Tool):

    def __call__(self):
        store = self.store
        if store.page is None:
            return "articleinfo.index.mako", store
        self.set_constants()
        self.prepare_query(store)

        loop = asyncio.get_event_loop()
        loop.run_until_complete(self.do_query(store.server, store.query))

        store.data = self.process(store.query)
        return "articleinfo.query.mako", store

    def set_constants(self):
        super().set_constants()
        store = self.store
        store.titleparts = self.titleparts(store.page)

        try:
            store.startdate = int(store.startdate) * 1000000
        except:
            store.startdate = 0

        try:
            store.enddate = int(store.enddate) * 1000000
        except:
            store.enddate = 0

        try:
            store.revlimit = int(store.revlimit)
            store.revlimit = min([store.revlimit, 100000])
        except:
            store.revlimit = 10000

        if store.revlimit < 3:
            raise ValueError("At least 3 revisions are required for analysis")

    @staticmethod
    def prepare_query(store: Databank):
        l = []
        s = []
        s.append(
            "SELECT r.rev_actor,r.rev_actor,r.rev_timestamp,r.rev_minor_edit,r.rev_id,"
            "rr.rev_len,r.rev_len,"
            "com.comment_text"
        )
        s.append("FROM revision_userindex r JOIN page ON rev_page=page_id")
        s.append("LEFT JOIN revision_userindex rr ON r.rev_parent_id=rr.rev_id")
        s.append("INNER JOIN comment com ON r.rev_comment_id=com.comment_id")
        s.append("WHERE page_title=%s")
        l.append(store.titleparts[2])
        s.append("AND page_namespace=%s")
        l.append(store.titleparts[0])
        if store.startdate:
            s.append("AND r.rev_timestamp > %s")
            l.append(str(store.startdate + 000000))
        if store.enddate:
            s.append("AND r.rev_timestamp < %s")
            l.append(str(store.enddate + 235959))
        s.append("ORDER BY r.rev_timestamp DESC LIMIT %s")
        l.append(store.revlimit)

        store.query.str = '\n'.join(s)
        store.query.args = tuple(l)

        l = []
        s = []
        s.append("SELECT COUNT(*)")
        s.append("FROM revision_userindex")
        s.append("JOIN page ON rev_page=page_id")
        s.append("WHERE page_title=%s")
        l.append(store.titleparts[2])
        s.append("AND page_namespace=%s")
        l.append(store.titleparts[0])

        store.query.str2 = '\n'.join(s)
        store.query.args2 = tuple(l)

    @staticmethod
    async def do_query(server, query):
        async with sql.connect(**connect_info(server)) as conn1:
            async with sql.connect(**connect_info(server)) as conn2:
                async with sql.connect(**connect_info(server)) as conn3:
                    cur1 = await conn1.cursor()
                    cur2 = await conn2.cursor()
                    cur3 = await conn3.cursor()

                    await asyncio.gather(
                            cur1.execute(query.str, query.args),
                            cur2.execute(query.str2, query.args2),
                        )

                    query.results = []
                    for row in await cur1.fetchall():
                        row = list(row)
                        query.results.append(row)
                    query.results.reverse()

                    actor_ids = {r[0] for r in query.results}
                    if not actor_ids:
                        raise Exception("This article has no revisions. Did you misspell the title? Incorrect capitalisation/lowercase? Is the article deleted?")
                    await cur3.execute("SELECT * FROM actor WHERE actor_id IN (%s)" % ','.join(map(str, actor_ids)))  # actor_id, actor_user, actor_name
                    actor_map = await cur3.fetchall()
                    actor_map = {a_id: (a_uid, a_name) for (a_id, a_uid, a_name) in actor_map}
                    for row in query.results:
                        row[0] = actor_map[row[0]][0]
                        row[1] = actor_map[row[1]][1]


                    query.revcount = (await cur2.fetchone())[0]

#if query.revcount < 3:
#    raise ValueError("At least 3 revisions are required for analysis")

    def process(self, query):
        # who the fuck knows wtf X! was thinking when he wrote his shit
        # gonna line-by-line this because fuck reading that
        data = {
            'first_edit': {
                'timestamp': None,
                'user': None,
            },
            'last_edit': {
                'timestamp': None,
                'user': None,
            },
            'year_count': collections.defaultdict(YearCounter),
            'count': 0,
            'total_count': 0,
            'editors': collections.defaultdict(UserContainer),
            'anons': [],
            'minor_count': 0,
            'count_history': CountCounter(),
        }
        first_edit_parse = Arrow.utcnow()
        for line in [query.results[0]]:
            line = [obj.decode("utf8") if isinstance(obj, bytes) else obj for obj in line]
            userid, user, ts, isminor, revid, oldsize, newsize, summ = line
            first_edit_parse = Arrow.strptime(str(ts), "%Y%m%d%H%M%S")
            data['first_edit']['timestamp'] = first_edit_parse
            data['first_edit']['user'] = user
        data['count'] = len(query.results)
        data['total_count'] = query.revcount
        for line in query.results:
            new_line = []
            for obj in line:
                if isinstance(obj, bytes):
                    try:
                        obj = obj.decode("utf8")
                    except Exception:
                        with open("lol.pkl", "wb") as fp:
                            import pickle
                            pickle.dump(line, fp)
                        obj = "failed to decode"
                new_line.append(obj)
            #rev_user_text, rev_timestamp, rev_minor_edit, rev_id, rev_len, rev_comment
            userid, user, ts, isminor, revid, oldsize, newsize, summ = new_line
            oldsize = oldsize or 0
            newsize = newsize or 0
            stamp = Arrow.strptime(str(ts), "%Y%m%d%H%M%S")
            data['last_edit']['timestamp'] = stamp
            data['last_edit']['user'] = user
            obj = data['year_count'][stamp.year]; del obj  # this activates the defaultdict
            data['year_count'][stamp.year].all += 1
            data['year_count'][stamp.year].months[stamp.month].all += 1
            data['year_count'][stamp.year].months[stamp.month].size.append(newsize)
            if data['editors'][user].all == 0:
                data['editors'][user].first = stamp
            data['editors'][user].all += 1
            data['editors'][user].last = stamp
            data['editors'][user].size.append(newsize)
            data['editors'][user].deltasize.append(newsize - oldsize)
            if not userid:
                data['anons'].append(user)
                data['year_count'][stamp.year].anon += 1
                data['year_count'][stamp.year].months[stamp.month].anon += 1
            if isminor:
                data['minor_count'] += 1
                data['year_count'][stamp.year].minor += 1
                data['year_count'][stamp.year].months[stamp.month].minor += 1
                data['editors'][user].minor += 1
            if Arrow.utcnow() - stamp < timedelta(days=1):
                data['count_history'].today += 1
            if Arrow.utcnow() - stamp < timedelta(days=7):
                data['count_history'].week += 1
            if Arrow.utcnow() - stamp < timedelta(days=30):
                data['count_history'].month += 1
            if Arrow.utcnow() - stamp < timedelta(days=365):
                data['count_history'].year += 1

        # clean up the chronological history chart thing
        for i in range(first_edit_parse.year, Arrow.utcnow().year + 1):
            i = data['year_count'][i]; del i  # activate the defaultdict
        # trim the first bit before the article got created
        prev_info = None
        for year, yearobj in sorted(data['year_count'].items()):
            for month, monthobj in enumerate(yearobj.months):
                if monthobj is None:
                    continue
                prev_info = year, month, monthobj
                if not monthobj.size:
                    yearobj.months[month] = None
                else:
                    # Because we're stopping when we reach the month with the first edit
                    break
            if prev_info is not None:
                y, m, m_obj = prev_info
                data['year_count'][y].months[m] = m_obj  # restore the prev info for padding
            break  # we know that the first year contains the first edit somewhere so this is safe
        for user, info in data['editors'].items():
            info.minorpct = info.minor / info.all * 100  # percent of minor edits
            info.addsize = sum(filter(lambda x: x > 0, info.deltasize))
            info.remsize = sum(filter(lambda x: x < 0, info.deltasize))
            #info.size.clear()  # bloop
            if info.all > 1:
                # slope = rise/run; rise is the total time difference, run is
                # the total number of edits, giving you average time
                # difference per edit. Consider as well the average value
                # theorem: 1/(b - a) * integral of f(x) dx from a to b
                info.atbe = timedelta(seconds=(info.last - info.first).total_seconds() / info.all)
                info.atbe = list(format_seconds(info.atbe, 2))
                for i, blob in enumerate(info.atbe):
                    if isinstance(blob, int):
                        info.atbe[i] = str(blob)
                    elif isinstance(blob, str):
                        info.atbe[i] += ','
                info.atbe = ' '.join(info.atbe)[:-1]  # remove the trailing comma
        # data['totaldays'] = int((data['last_edit']['timestamp'] - first_edit_parse).total_seconds() / 60 / 60 / 24)
        data['totaldays'] = (data['last_edit']['timestamp'] - first_edit_parse).days
        data['average_days_per_edit'] = data['totaldays'] / data['count']
        data['edits_per_month'] = data['count'] / (data['totaldays'] / (365/12)) if  data['totaldays'] else 0
        data['edits_per_year'] = data['count'] / (data['totaldays'] / 365) if  data['totaldays'] else 0
        data['edits_per_editor'] = data['count'] / len(data['editors'])
        data['editor_count'] = len(data['editors'])
        data['anon_count'] = len(data['anons'])
        return data
