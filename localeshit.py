#!/home/sigma/.local/bin/python

import collections

CH_LOC = 'ch_locale'

__all__ = 'diff', 'hist', 'minor', 'top', 'diffbyte', 'name', 'timefmt', CH_LOC

def ch_locale(lang, func, args=(), kwargs=None):
    import locale
    import multiprocessing as mp

    loc = name[lang]

    if kwargs is None:
        kwargs = {}
    def sandbox(the_queue, errors):
        saved = locale.setlocale(locale.LC_ALL)
        try:
            locale.setlocale(locale.LC_ALL, loc)
            the_queue.put(func(*args, **kwargs))
        except Exception as e:
            errors.put(e)
            the_queue.put(None)
        else:
            errors.put(None)
        finally:
            locale.setlocale(locale.LC_ALL, saved)

    q = mp.Queue()
    errq = mp.Queue()
    p = mp.Process(target=sandbox, args=[q, errq])
    p.start()
    ret = q.get()
    err = errq.get()
    p.join()
    if err:
        raise err
    return ret

#import arrow
#print(ch_locale('de', arrow.utcnow().strftime, ["%A"]))


diff = {
    'de': 'Unterschied', 'it': 'diff', 'pl': 'różn.',
    'commons': 'diff', 'ja': '差分', 'en': 'diff',
    'fr': 'diff', 'zh': '差异', 'ru': 'обсуждение',
    'es': 'dif', 'nl': 'wijz', 'pt': 'dif'
}
diff = collections.defaultdict(lambda: "diff", diff)

hist = {
    'de': 'Versionen', 'it': 'cron', 'pl': 'hist.', 'commons': 'hist',
    'ja': '履歴', 'en': 'hist', 'fr': 'hist', 'zh': '历史',
    'ru': 'вклад', 'es': 'hist', 'nl': 'gesch', 'pt': 'his'
}
hist = collections.defaultdict(lambda: "hist", hist)

minor = {
    'de': 'K', 'it': 'm', 'pl': 'm', 'commons': 'm', 'ja': 'm', 'en': 'm',
    'fr': 'm', 'zh': '小', 'ru': 'м', 'es': 'm', 'nl': 'k', 'pt': 'm'
}
minor = collections.defaultdict(lambda: "m", minor)

top = {
    'de': 'aktuell',
    'eo': 'aktuala',
    'es': 'edición actual',
    'fr': 'actuel',
    'it': 'attuale',
    'ja': '最新',
    'nl': 'laatste wijziging',
    'pl': 'ostatnia',
    'pt': 'atual',
    'ru': 'текущая',
    'zh': '当前'
}
top = collections.defaultdict(lambda: 'top', top)

diffbyte = {
    "de": "Bytes nach der Änderung", "en": "bytes after change",
    "fr": "octets après changement", "es": "bytes después del cambio",
    "it": "byte dopa la modifica", "pl": "bajtów po zmianie",
    "ru": "Размер после изменения: %d байт",
    "zh": "更改后%d字节", "ja": "変更後は %d バイト",
    "pt": "bytes após mudança", "nl": "bytes na de wijziging",
}
diffbyte = {k: "%d " + v if "%d" not in v else v for k, v in diffbyte.items()}
diffbyte = collections.defaultdict(lambda: "%d bytes after change", diffbyte)

# localename = {'de': 'de', 'it': 'it', 'pl': 'pl', 'commons': 'en', 'ja': 'ja', 'en': 'en',
#               'fr': 'm', 'zh': 'zh', 'ru': 'ru', 'es': 'es', 'nl': 'nl', 'pt': 'pt'
# }
name = {
    'de': 'de_DE', 'it': 'it_IT', 'pl': 'pl_PL',
    'ja': 'ja_JP', 'en': 'en_GB', 'commons': 'en_GB',
    'fr': 'fr_FR', 'zh': 'zh-hant_TW', 'ru': 'ru_RU',
    'es': 'es_ES', 'nl': 'nl_BE', 'pt': 'pt_PT'
}
name = {k: v + ".utf8" for k, v in name.items()}
name = collections.defaultdict(lambda: "en_GB.utf8", name)

timefmt = {
    'de': "%H:%M, %d. %b %Y", 'it': "%H:%M, %d %b %Y",
    'commons': "%H:%M, %d %B %Y", 'ja': '%Y年%b月%d日 (%a) %H:%M',
    'en': "%H:%M, %d %B %Y", 'fr': '%d %B %Y à %H:%M',
    'zh': '%x (%a) %H:%M', 'ru': '%H:%M, %d %B %Y',
    'es': '%H:%M %d %b %Y', 'nl': '%d %b %Y %H:%M',
    'pt': "%Hh%Mmin de %d %B de %Y", 'pl': "%H:%M, %d %b %Y",
}
timefmt = collections.defaultdict(lambda: "%H:%M, %d %B %Y", timefmt)
