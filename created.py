#!/home/sigma/.local/bin/python3
# -*- coding: utf-8 -*-
# Copyright (c) 2015 User:Σ, GPLv3

import aiomysql as sql
import asyncio

from utils import Databank, connect_info
from tool import Tool


class CreatedPages(Tool):

    def __call__(self):
        store = self.store
        if store.name is None:
            return "created.index.mako", store
        self.set_constants()
        self.prepare_query(store)

        loop = asyncio.get_event_loop()
        loop.run_until_complete(self.do_query(store.server, store.query))

        store.query.results = self.fix_results(store.query.rawresults)
        return "created.query.mako", store

    @staticmethod
    def rd_qstring(rd_opt):
        if rd_opt == "rd":
            return "AND page_is_redirect=1", "only redirects"
        if rd_opt == "none":
            return "AND page_is_redirect=0", "non-redirects"
        return '', "pages"

    def set_constants(self):
        super().set_constants()
        store = self.store
        store.redirects, store.redirects_desc = self.rd_qstring(store.redirects)

        try:
            store.max = int(store.max)
            store.max = min([store.max, 500])
        except:
            store.max = 500

        try:
            store.startdate = int(store.startdate) * 1000000
        except:
            store.startdate = 0

        rvns = self.store.rvnamespaces
        if store.ns:
            ns_ = {rvns[x.lower()] for x in store.ns.split(",") if x.lower() in rvns}
            if ns_:
                # Only do it if there were valid namespaces
                store.ns_ = ", ".join("%s" % e for e in ns_)

    @staticmethod
    def prepare_query(store: Databank):
        l = []
        s = []
        if store.deleted in {'all', 'undeleted'}:
            s.append("SELECT rev_id, rev_len, page_title, page_namespace, rev_timestamp, com.comment_text, page_is_redirect, 1")
            s.append("FROM revision_userindex JOIN page ON page_id=rev_page")
            s.append("INNER JOIN comment com ON rev_comment_id=com.comment_id")
            s.append("WHERE rev_actor IN (SELECT actor_id FROM actor WHERE actor_name=%s) AND rev_parent_id=0")
            l.append(store.name)
            if store.redirects:
                s.append(store.redirects)
            if store.ns_:
                s.append("AND page_namespace IN (%s)" % store.ns_)
            if store.startdate:
                s.append("AND rev_timestamp <= %s")
                l.append(str(store.startdate))
        if store.deleted == 'all':
            s.append("UNION")
        if store.deleted in {'all', 'deleted'}:
            s.append("SELECT ar_id, ar_len, ar_title, ar_namespace, ar_timestamp, com.comment_text, 0, 0")
            s.append("FROM archive_userindex")
            s.append("INNER JOIN comment com ON ar_comment_id=com.comment_id")
            s.append("WHERE ar_actor IN (SELECT actor_id FROM actor WHERE actor_name=%s) AND ar_parent_id=0")
            l.append(store.name)
            # round two
            if store.ns_:
                s.append("AND ar_namespace IN (%s)" % store.ns_)
            if store.startdate:
                s.append("AND ar_timestamp <= %s")
                l.append(store.startdate)

        if store.deleted == 'deleted':
            s.append("ORDER BY ar_timestamp DESC")
        else:
            s.append("ORDER BY rev_timestamp DESC")
        s.append("LIMIT %s")
        l.append(store.max)
        store.query.str = '\n'.join(s)
        store.query.args = tuple(l)

        # prepare for the COUNT(*)
        del s[-1]  # LIMIT
        del l[-1]
        del s[-1]  # ORDER BY
        for i, piece in enumerate(s):
            # this part sucks and I feel bad but WHAT ELSE AM I SUPPOSED TO DO
            if piece.lower().startswith("select"):
                s[i] = "SELECT COUNT(*)"
        store.query.str2 = '\n'.join(s)
        store.query.args2 = tuple(l)

    @staticmethod
    async def do_query(server, query):
        async with sql.connect(**connect_info(server)) as conn1:
            async with sql.connect(**connect_info(server)) as conn2:
                cur1 = await conn1.cursor()
                cur2 = await conn2.cursor()

                await asyncio.gather(cur1.execute(query.str, query.args), cur2.execute(query.str2, query.args2))

                query.rawresults = await cur1.fetchall()
                query.size = (await cur2.fetchone())[0]

    def fix_results(self, res):
        for line in res:
            new_line = []
            for obj in line:
                if isinstance(obj, bytes):
                    obj = obj.decode("utf8")
                new_line.append(obj)
            # this next part is the worst part ever
            revid, size, title, namespace, ts, summ, is_redirect, exists = new_line
            is_redirect = "(redirect)" if is_redirect else ''
            # lamest of the lame
            # parentid, revid, ts, isminor, summ, oldsize, newsize, title, namespace, topid
            args = 0xdeadbeef, revid, ts, False, summ, 0, size, title, namespace, False
            urls, ts, stamp, diffword, histword, sizetag, minor, page, summ, top = self.contrib_line(args)
            yield urls, ts, stamp, diffword, histword, sizetag, page, summ, is_redirect, exists
