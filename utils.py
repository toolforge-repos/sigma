#!/home/sigma/.local/bin/python3
# -*- coding: utf-8 -*-
# Copyright (c) 2015 User:Σ, GPLv3

import re
import stats
from datetime import timedelta

import markupsafe

_summary_regex = re.compile(r"/\*(.*?)\*/")

def abstract_quartiles(data):
    if not len(data):
        return [0, 0, 0]
    if len(data) < 3:
        return [min(data), stats.median(data), max(data)]
    return list(stats.quartiles(data))


def fmt_summ(s):
    if s is None:
        return ''
    s = str(markupsafe.escape(s.replace("\n", '')))
    return _summary_regex.sub(r"<span class=gray>/*\1*/</span>", s)


def connect_info(dbname):
    import configparser

    config = configparser.ConfigParser()
    config.read("/data/project/sigma/replica.my.cnf")
    sqlu = config['client']['user'][1:-1]
    sqlp = config['client']['password'][1:-1]
    if dbname.endswith('_p'):
        dbname = dbname[:-2]
    if dbname == 'meta':
        host = 'enwiki.analytics.db.svc.wikimedia.cloud'
    else:
        host = dbname + ".analytics.db.svc.wikimedia.cloud"
    return dict(db=dbname + "_p", host=host, user=sqlu, password=sqlp)


def format_seconds(seconds, chain=0):
    if isinstance(seconds, timedelta):
        seconds = seconds.total_seconds()
    units = {
        "years": 365 * 24 * 60 * 60, "days": 24 * 60 * 60,
        "hours": 60 * 60, "minutes": 60, "seconds": 1
    }
    # Deconstruct the dict into a list of 2-tuples
    # We should probably use an OrderedDict for this or something
    units = list(reversed(sorted(units.items(), key=lambda tp: tp[1])))
    ret = []
    for i, (smallest_u, v) in enumerate(units):
        if seconds / v >= 1:
            for unit, divisor in units[i:]:
                thing, seconds = divmod(seconds, divisor)
                ret.append((unit, int(thing)))  # [(minutes, 10), (seconds, 2)]
            if chain:
                realret = []
                for j, (unit, number) in enumerate(ret):
                    if j > chain:
                        break
                    realret.append(number)
                    realret.append(unit)
                return tuple(realret)
            return dict(ret)[smallest_u], smallest_u
    return 0, 'seconds'


def destampify(ts: timedelta):
    return ts.strftime("%Y%m%d%H%M%S")

def ucfirst(s: str, limit=1, c=0):
    """Now with super awesome namespace checks"""
    if ":" in s and c < limit:
        if s.count(":") < 1:
            return s
        parts = s.split(":")
        first = parts.pop(0)
        return ucfirst(first) + ":" + ucfirst(":".join(parts), limit=limit, c=c+1)
    return s[0].upper() + s[1:] if len(s) else s


class Databank:
    """It's basically just a dict with attributes instead of keys."""
    def __init__(self, load=None):
        self._data = {}
        if load:
            for k, v in load.items():
                self._data[k] = v

    def __getattr__(self, item):
        return self._data.get(item)

    def __setattr__(self, key, value):
        if key == '_data':
            super().__setattr__(key, value)
            return
        self._data[key] = value

    def __delattr__(self, item):
        del self._data[item]
