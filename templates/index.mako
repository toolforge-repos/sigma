<!doctype html>
<html>
<head>
    <%include file="header.mako" args="title='&Sigma; on Toolforge', scripts=False" />
    <script type=text/javascript>
if (window.location.href.substr(-1) != '/') window.location.href += '/';
    </script>
</head>
<body>
    <p>I maintain several tools on this website.</p>
    <!--<p>You can view their source by sticking <samp>/source</samp> at the
    end of their URLs.
    For example, <a href=./created.py/source>here</a>.</p>-->
    <ul>
        <li><a href=summary.py>Edit summary search</a></li>
        <li>
            <a href=editorinteract.py>Editor interaction analyser</a>
            <ul>
                <li><a href=timeline.py>Timeline supplement</a></li>
            </ul>
        </li>
        <li><a href=created.py>Pages created</a></li>
        <li><a href=usersearch.py>User contribution search</a></li>
        <li><a href=articleinfo.py>Article revision statistics</a></li>
    </ul>
    <p>There exists a number of tools that I maintain, but are not fully
    integrated into my administration. In addition, the source-viewing
    function has not been added to these yet.</p>
    <ul>
        <li><a href=/afdstats>AfD Statistics</a></li>
    </ul>
    <%include file="footer.mako" />
</body>
</html>
