<!doctype html>
<%!
    import time, arrow
    from urllib.parse import urlencode
    from utils import destampify
%>
<html>
<head>
    <%include file="header.mako" args="title='User Contribution Search'" />
</head>
<body>
    <a href=../><small>&larr;New search</small></a>
    <br><br>

    <form action=usersearch.py method=GET>
        <ul style="list-style-type:none">
            <li>
                <label>Username:&nbsp;<input type=text id=name name=name value="${store.name|h}" /></label>
                <label>Page:&nbsp;<input type="text" id=page name="page" size="40" /></label>
                <label>Max&nbsp;edits:&nbsp;<input type="text" name="max" maxlength="3" size="5" value="500" /></label>
            </li>
            <li><label>Database:&nbsp;<input type=text id=server name=server value="${store.server|h}" size=10 /></label></li>
            <li><label><input type=checkbox name=noredirect disabled />&nbsp;Don't resolve redirects</label></li>
            <li><label><input type=checkbox name=wildcards disabled />&nbsp;Allow wildcards in page title (*)</label></li>
            <li><label><input type=checkbox name=casesensitive disabled />&nbsp;Page title is case sensitive (only applies when wildcards are used)</label></li>
            <li><input type=submit value=Submit /></li>
        </ul>
    </form>
    <br />
    <hr style="text-align:left;width:875px;margin-left:0">
    <br />

    <p>
        Found ${store.query.count_res_user} edits by <b>${store.name|h}</b>
        on ${store.page.replace("_", " ")|h}
        %if store.query.count_res_total:
            (${round(100 * store.query.count_res_user / store.query.count_res_total, 2)}% of the total edits made to the page)
        %else:
            (NaN% of the total edits made to the page)
        %endif
    </p>
    <ul>
        %for u, ts, stamp, diff, hist, sizetag, minor, title, summ, top in store.query.results:
            <li>
                <a href="${u['rv']|h}">${stamp}</a> (<a href="${u['diff']|h}">${diff}</a> | <a href="${u['hist']|h}">${hist}</a>)
                . . ${sizetag} . .
                ${minor}
                <a href="${u['']|h}">${title|h}</a>
                %if summ:
                    (<i>${summ}</i>)
                %endif
                ${top}
            </li>
            <% store.continue_date = destampify(ts) %>
        %endfor
    </ul>
    <%
        params = {
            "startdate": str(store.continue_date),
            "name": store.name,
            "page": store.page,
            "server": store.server,
            "max": store.max,
        }
        params = urlencode(params)
    %>
    <a href="usersearch.py?${params|h}"><small>Next ${store.max} results &rarr;</small></a><br />
    <br />
    <small>
        Elapsed time: ${round(time.time() - store.starttime, 3)} seconds.
        <br />
        ${arrow.now().strftime("%H:%M:%S, %d %b %Y")}
    </small>
    <br />
    <br />
    <a href="usersearch.py"><small>&larr;New search</small></a>
    <%include file="footer.mako" />
</body>
</html>
