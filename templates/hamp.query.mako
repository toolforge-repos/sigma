<!doctype html>
<% 
import random, langdetect
import indicoio as indico
%>
<html>
<head>
    
    <meta http-equiv=Content-Type content="text/html; charset=UTF-8" />
    <link href=https://tools.wmflabs.org/static/common.css rel=stylesheet type=text/css />
    <link href=https://tools.wmflabs.org/static/diff.css rel=stylesheet type=text/css />
    <title>Hack the hackathon</title>
    <script type=text/javascript src=https://tools.wmflabs.org/static/res/jquery/2.1.0/jquery.min.js></script>
    <script type=text/javascript>
        var urlParams={};(function(){var a,b=/\+/g,c=/([^&=]+)=?([^&]*)/g,d=function(a){return decodeURIComponent(a.replace(b," "))},e=window.location.search.substring(1);while(a=c.exec(e))urlParams[d(a[1])]=d(a[2])})();
        // http://stackoverflow.com/questions/901115/how-can-i-get-query-string-values
        function setTheParams() {
            var elem;
            for (var key in urlParams) {
                elem = $("#" + key);
                if (elem.prop("type") == 'checkbox')
                    elem.prop("checked", 1);
                else
                    elem.val(urlParams[key]);
            }
        }

        $(document).ready(setTheParams);
    </script>

</head>
<body>
    <h1>Enter your hackathon project idea</h1>
    <p>This will predict whether or not your hackathon project will win</p>
    <hr style="text-align:left;width:875px;margin-left:0">
    <% res = indico.language(store.desc, api_key="760014c6e74c4586a6ca95738360477a") %>
    %if max(res, key=res.get) != "English":
    <!--%if langdetect.detect(store.desc) != "en" or max(res, key=res.get) != "English":-->
    Your project is <strong>NOT IN ENGLISH</strong> and has not been scored.
    %else:
    Your project <strong>${"SUCKS" if random.random() < 0.5 else "ROCKS"}</strong>.
    %endif
    <div id=footer>
    <small>
        &copy;&nbsp;2016&nbsp;MIT Rejects
    </small>
    </div>

</body>
</html>

