<!doctype html>
<html>
<head>
<%include file="header.mako" args="title='User Contribution Search'" />
</head>
<body onload="initForm();">
    <h1>User Contribution Search</h1>
    <p>This tool will search through a page's history and return edits made
       by a particular user on that page.</p>
    <p>Expand the "Help &amp; Tips"
       section at the bottom of this page for help with using
       this tool effectively.</p>
    <div id=indexform>
        <form action="usersearch.py" method=GET>
            <div id="formalign">
                <label for=name>Username:</label>
                <input type=text id=name name=name autofocus />
                <label for=page>Page:</label>
                <input type=text id=page name=page />
                <label for=server>Database:</label>
                <input type=text id=server name=server value=enwiki />
                <label for=max>Max number of edits to return:
                    <span class="small">(default=100, max=500)</span>
                </label>
                <input type=text id=max name=max maxlength=3 size=5 />
            </div>
            <div style="clear:both; padding-top:10px;">
                <label for=noredirect>Don't resolve redirects</label>
                <input type=checkbox id=noredirect name=noredirect disabled />
                <label for=wildcards>Allow wildcards in page title (*)</label>
                <input type=checkbox id=wildcards name=wildcards disabled />
                <label for=casesensitive>Page title is case sensitive (only applies when wildcards are used)</label>
                <input type=checkbox id=casesensitive name=casesensitive disabled />
                <button type="submit">Submit</button>
            </div>
        </form>
    </div>
    <br/>
    <a onclick="$('#helpdiv').toggle()">Help &amp; Tips</a>
    <div id="helpdiv" style="display:none">
        <ul>
            <li><b>Don't resolve redirects</b> &mdash; Normally, the tool will
                try to resolve redirects, so if you use &quot;WP:ANI&quot; for the
                page name, it will resolve it to &quot;Wikipedia:Administrators'
                noticeboard/Incidents&quot;.  However, in the rare case that you
                want to list a user's contributions to a page that is
                currently a redirect, check this checkbox.</li>
            <li><b>Allow wildcards in page title</b> &mdash; To search through a
                range of pages, use asterisks in the page name for wildcards.
                Note that wildcards do not work in the namespace, so
                &quot;User*:Snottywong&quot; won't return edits from
                &quot;User:Snottywong&quot; and &quot;User talk:Snottywong&quot;.
                Use wildcards only after the colon.</li>
            <li><b>Page title is case sensitive</b> &mdash; When wildcards are
                enabled, the pages search is not case sensitive unless this
                checkbox is checked.  This option has no effect when
                wildcards are not enabled.</li>
        </ul>
    </div>
    <br />
    <br />
    <small>
        <% import time, arrow %>
        Elapsed time: ${round(time.time() - store.starttime, 3)} seconds.
        <br />
        ${arrow.now().strftime("%H:%M:%S, %d %b %Y")}
    </small>
    <%include file="footer.mako" />
</body>
</html>
