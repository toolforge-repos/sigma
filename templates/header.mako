<%page args="title='Untitled page', scripts=True" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link href="/static/common.css" rel="stylesheet" type="text/css" />
    <link href="/static/diff.css" rel="stylesheet" type="text/css" />
    <title>${title}</title>
%if scripts:
    <!--<script type="text/javascript" src="//static.toolforge.org/res/jquery/2.1.0/jquery.min.js"></script>-->
    <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
    <script type="text/javascript">
        var urlParams={};(function(){var a,b=/\+/g,c=/([^&=]+)=?([^&]*)/g,d=function(a){return decodeURIComponent(a.replace(b," "))},e=window.location.search.substring(1);while(a=c.exec(e))urlParams[d(a[1])]=d(a[2])})();
        // http://stackoverflow.com/questions/901115/how-can-i-get-query-string-values
        function setTheParams() {
            var elem;
            for (var key in urlParams) {
                elem = $("#" + key);
                if (elem.prop("type") == 'checkbox')
                    elem.prop("checked", 1);
                else
                    elem.val(urlParams[key]);
            }
        }

        $(document).ready(setTheParams);
    </script>
%endif
