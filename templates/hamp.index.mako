<!doctype html>

<html>
<head>
    
    <meta http-equiv=Content-Type content="text/html; charset=UTF-8" />
    <link href=https://tools.wmflabs.org/static/common.css rel=stylesheet type=text/css />
    <link href=https://tools.wmflabs.org/static/diff.css rel=stylesheet type=text/css />
    <title>Article revision statistics</title>
    <script type=text/javascript src=https://tools.wmflabs.org/static/res/jquery/2.1.0/jquery.min.js></script>
    <script type=text/javascript>
        var urlParams={};(function(){var a,b=/\+/g,c=/([^&=]+)=?([^&]*)/g,d=function(a){return decodeURIComponent(a.replace(b," "))},e=window.location.search.substring(1);while(a=c.exec(e))urlParams[d(a[1])]=d(a[2])})();
        // http://stackoverflow.com/questions/901115/how-can-i-get-query-string-values
        function setTheParams() {
            var elem;
            for (var key in urlParams) {
                elem = $("#" + key);
                if (elem.prop("type") == 'checkbox')
                    elem.prop("checked", 1);
                else
                    elem.val(urlParams[key]);
            }
        }

        $(document).ready(setTheParams);
    </script>

</head>
<body>
    <h1>Enter your hackathon project idea</h1>
    <p>This will predict whether or not your hackathon project will win</p>
    <div id=indexform>
    <form action=https://tools.wmflabs.org/submit.py method=POST>
	<label for=image> Does the project have an image?</label>
	<input name= image id=image type=checkbox />
	<label for=video> Does the project have a video?</label>
	<input name= image id=image type=checkbox />
        <textarea id=desc cols="50" rows="10" name=desc placeholder="Hackathon project description"></textarea>
           <button>Submit</button>
        <div style='clear:both'></div>
    </form>
    </div>
    <div id=footer>
    <small>
        &copy;&nbsp;2016&nbsp;MIT Rejects
    </small>
    </div>

</body>
</html>

