#!/home/sigma/.local/bin/python3
# -*- coding: utf-8 -*-
# Copyright (c) 2015 User:Σ, GPLv3


import asyncio

import aiomysql as sql

from utils import connect_info, ucfirst
from tool import Tool
import localeshit

ch_locale = localeshit.ch_locale


class UserSearch(Tool):

    def __call__(self):
        store = self.store
        if store.name is None or store.page is None:
            return "usersearch.index.mako", store
        self.set_constants()
        self.prepare_query(store)

        loop = asyncio.get_event_loop()
        loop.run_until_complete(self.do_query(store.server, store.query))

        store.query.results = self.fix_results(store.query.rawresults)
        return "usersearch.query.mako", store

    def set_constants(self):
        super().set_constants()
        store = self.store

        store.name = ucfirst(store.name.strip())
        store.page = ucfirst(store.page)
        store.titleparts = self.titleparts(store.page)

        try:
            store.max = int(store.max)
            store.max = min([store.max, 500])
        except:
            store.max = 500

        try:
            store.startdate = int(store.startdate)
        except:
            store.startdate = 0


    @staticmethod
    def prepare_query(store):
        l = []
        s = []
        s.append(
            "SELECT rr.rev_parent_id,r.rev_id,r.rev_timestamp,r.rev_minor_edit,com.comment_text,rr.rev_len,r.rev_len,"
            "page_latest"
        )
        s.append("FROM revision_userindex r JOIN page ON rev_page=page_id")
        s.append("LEFT JOIN revision_userindex rr ON r.rev_parent_id=rr.rev_id")
        s.append("INNER JOIN comment com ON r.rev_comment_id=com.comment_id")
        # long gone are the days where sql queries were easy :(
        #s.append("SELECT rev_id,rev_timestamp,rev_minor_edit,rev_comment")
        #s.append("page_latest")
        #s.append("FROM revision_userindex JOIN page ON rev_page=page_id")
        s.append("WHERE r.rev_deleted=0")
        s.append("AND r.rev_actor IN (SELECT actor_id FROM actor WHERE actor_name=%s)")
        l.append(store.name)
        s.append("AND page_namespace=%s")
        l.append(store.titleparts[0])
        s.append("AND page_title=%s")
        l.append(store.titleparts[2])
        if store.startdate:
            s.append("AND r.rev_timestamp <= %s")
            l.append(str(store.startdate))
        s.append("ORDER BY r.rev_timestamp DESC")
        s.append("LIMIT %s")
        l.append(store.max)
        store.query.str = '\n'.join(s)
        store.query.args = tuple(l)

        s = []
        l = []
        s.append("SELECT COUNT(*)")
        s.append("FROM revision_userindex")
        s.append("JOIN page ON rev_page=page_id")
        s.append("WHERE rev_actor IN (SELECT actor_id FROM actor WHERE actor_name=%s)")
        l.append(store.name)
        s.append("AND page_title=%s")
        l.append(store.titleparts[2])
        s.append("AND page_namespace=%s AND rev_deleted=0")
        l.append(store.titleparts[0])
        store.query.count_str = '\n'.join(s)
        store.query.count_args = tuple(l)

        s = []
        l = []
        s.append("SELECT COUNT(*)")
        s.append("FROM revision_userindex")
        s.append("JOIN page ON rev_page=page_id")
        s.append("WHERE page_title=%s")
        l.append(store.titleparts[2])
        s.append("AND page_namespace=%s")
        l.append(store.titleparts[0])
        s.append("AND rev_deleted=0")
        store.query.count_str2 = '\n'.join(s)
        store.query.count_args2 = tuple(l)

    @staticmethod
    async def do_query(server, query):
        async with sql.connect(**connect_info(server)) as conn1:
            async with sql.connect(**connect_info(server)) as conn2:
                async with sql.connect(**connect_info(server)) as conn3:
                    cur1 = await conn1.cursor()
                    cur2 = await conn2.cursor()
                    cur3 = await conn3.cursor()

                    await asyncio.gather(
                            cur1.execute(query.str, query.args),
                            cur2.execute(query.count_str, query.count_args),
                            cur3.execute(query.count_str2, query.count_args2),
                        )

                    query.rawresults = await cur1.fetchall()
                    query.count_res_user = (await cur2.fetchone())[0]
                    query.count_res_total = (await cur3.fetchone())[0]

    def fix_results(self, res):
        ns, crap, title = self.titleparts(self.store.page)
        for line in res:
            new_line = []
            for obj in line:
                if isinstance(obj, bytes):
                    obj = obj.decode("utf8")
                new_line.append(obj)
            # this next part is the worst part ever
            parentid, revid, ts, isminor, summ, oldsize, newsize, topid = new_line
            # let's use contrib_line for this shit
            # parentid, revid, ts, isminor, summ, oldsize, newsize, title, namespace, topid
            args = parentid, revid, ts, isminor, summ, oldsize, newsize, title, ns, topid
            yield self.contrib_line(args)
