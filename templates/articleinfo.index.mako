<!doctype html>
<%!
    import time, arrow
%>
<html>
<head>
    <%include file="header.mako" args="title='Article revision statistics'" />
</head>
<body>
    <h1>Article revision statistics</h1>
    <p>This tool displays various statistics about the history of an article.</p>
    <div id=indexform>
    <form action=/articleinfo.py method=GET>
        <label for=page>Page:</label>
        <input type=text id=page name=page />
        <label for=startdate>
            Start date:<br/>
            <span class=gray>YYYYMMDD format</span>
        </label>
        <input type=text id=startdate name=startdate />
        <label for=enddate>End date:</label>
        <input type=text id=enddate name=enddate />
        <label for=revlimit>
            Number of revisions to analyse:<br/>
            <span class=gray>max=100000</span>
        </label>
        <input type=text id=revlimit name=revlimit value=10000 />
        <label for=server>Database:<br/>
            <span class=gray>lang + project (<a href="//meta.wikimedia.org/w/api.php?action=sitematrix&format=jsonfm">list</a>)</span>
        </label>
        <input type=text id=server name=server value=enwiki />

        <button>Submit</button>
        <div style='clear:both'></div>
    </form>
    </div>
    <br />
    <small>
        Elapsed time: ${round(time.time() - store.starttime, 3)} seconds.
        <br />
        ${arrow.now().strftime("%H:%M:%S, %d %b %Y")}
    </small>
    <br />
    <br />
    <a href="/"><small>&larr;Home</small></a>
    <%include file="footer.mako" />
</body>
</html>

