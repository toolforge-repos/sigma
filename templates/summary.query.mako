<!doctype html>
<%
    import time, arrow
    from urllib.parse import urlencode
    from utils import destampify
%>
<html>
<head>
    <%include file="header.mako" args="title='Edit summary search'" />

<style>
    .advanced {
        display: none;
    }
</style>
</head>
<body>
    <h1>Edit summary search</h1>
    <p>This tool searches through a user's contribution history and returns
    edits made by that user if the edit summary contains the specified string.</p>

    <div>
        <form action=summary.py method=GET>
            <ul style="list-style-type:none">
                <li>
                    <label>Username:&nbsp;<input type=text id=name name=name value="${store.name|h}" /></label>
                    <label>Search:&nbsp;<input type="text" id=search name=search size="40" value="${store.search|h}" /></label>
                    <label>Max&nbsp;pages:&nbsp;<input type="text" name="max" maxlength="3" size="5" value="${store.max|h}" /></label>
                </li>
                <li><label>Database:&nbsp;<input type=text id=server name=server value="${store.server|h}" size=10 /></label></li>
                <li><a onclick='$(".advanced").toggle()'>Advanced&nbsp;options &darr;</a></li>
                    <li class=advanced><label>Restrict to namespaces:&nbsp;<input type='text' id='ns' name='ns' placeholder='Talk,Book talk,,' value="${store.ns|h}" /></label></li>
                    <!-- intentionally in the reverse order; the tool interprets "start date" in reverse chronological order -->
                    <li class=advanced><label>From date:&nbsp;<input type='text' id='enddate' name='enddate' placeholder='YYYYMMDDHHMMSS' value="${store.enddate if store.enddate else ''|h}" /></label>&nbsp;<label>To date:&nbsp;<input type='text' id='startdate' name='startdate' placeholder='YYYYMMDDHHMMSS' value="${store.startdate if store.startdate else ''|h}" /></label></li>
                    <li class=advanced><label><input type='checkbox' id='nosect' name='nosect' />Don't search within /* sections */</label></li>
                    <li class=advanced><label><input type='checkbox' id='casesensitive' name='casesensitive' />Case sensitive search</label></li>
                <li><input type=submit value=Submit /></li>
            </ul>
        </form>
    </div>

    <hr style="text-align:left;width:875px;margin-left:0">
    <p>(${store.query.size} remaining)</p>
    <ul>
        % for urls, ts, stamp, diffword, histword, sizetag, minor, title, summ, top in store.query.results:
            <li>
                <a href="${urls['rv']|h}">${stamp}</a>
                (<a href="${urls['diff']|h}">${diffword}</a> | <a href="${urls['hist']|h}">${histword}</a>)
                . . ${sizetag} . .
                ${minor}
                <a href="${urls['']|h}">${title|h}</a> <i>(${summ})</i> ${top}
            </li>
            <% store.continue_date = destampify(ts) %>
        % endfor
    </ul>
    <%
        params = {
            "startdate": str(store.continue_date),
            "name": store.name,
            "server": store.server,
            "max": store.max,
            "search": store.search,
            "nosect": store.nosect,
            "casesensitive": store.casesensitive,
            "ns": store.ns
        }
        deleteme = set()
        for k, v in params.items():
            if not v:
                deleteme.add(k)
        for k in deleteme:  # avoid modifying an object when iterating over it
            del params[k]
        params = urlencode(params)
    %>
        <a href="summary.py?${params|h}"><small>Next ${store.max} results &rarr;</small></a><br />
    <span id='warnings'>
        <!--${store.query.str}-->
    </span>
    <br />
    <small>
        Elapsed time: ${round(time.time() - store.starttime, 3)} seconds.
        <br />
        ${arrow.now().strftime("%H:%M:%S, %d %b %Y")}
    </small>
    <br />
    <br />
    <a href="summary.py"><small>&larr;New search</small></a>
    <%include file="footer.mako" />
</body>
</html>

