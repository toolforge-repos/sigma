#!/home/sigma/.local/bin/python3

import json
import string
from pathlib import *

__all__ = 'get_namespaces', 'get_domain'

#p = "/data/project/sigma/cherry/cherryhtml/metacrap"
#p = '/home/andrew/Dropbox/1/xtools/cherryhtml/metacrap'
p = "/data/project/sigma/www/python/src/metacrap"
p = Path(p).resolve()


def get_crap(dbname):
    p_ = (p/(dbname+'.meta')).resolve()

    try:
        assert all(ch not in './' for ch in dbname)
        p_.relative_to(p)
    except (ValueError, AssertionError):
        raise ValueError("nice try")

    with p_.open() as fp:
        data = json.load(fp)
    return data

def get_namespaces(dbname):
    data = get_crap(dbname)['namespaces']
    return {int(k): v for k, v in data.items()}

def get_domain(dbname):
    return get_crap(dbname)['url']

def get_lang(dbname):
    return get_crap(dbname)['lang']
