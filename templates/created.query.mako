<!doctype html>
<%!
    import time
    import arrow
    from urllib.parse import urlencode
    from utils import destampify
%>
<html>
<head>
<%include file="header.mako" args="title='Pages created'" />
</head>
<body>
    <h1>Pages created</h1>
% if store.error:
    <div class=mbox>${store.error|h}</div>
    <!-- ${store.errstr} -->
% endif
    <p>This tool lists pages created by a user.</p>

    <hr style="text-align:left;width:875px;margin-left:0">
    % if store.query.results and 1:
        <p>Showing ${store.redirects_desc} (${store.query.size} remaining) created by <b>${store.name|h}</b></p>
        <ul>
            % for urls, ts, stamp, diffword, histword, sizetag, title, summ, is_redirect, exists in store.query.results:
                <li>
                    % if not exists:
                        <s>
                    % endif
                    <a href="${urls['rv']|h}">${stamp}</a>
                    (${diffword} | <a href="${urls['hist']|h}">${histword}</a>)
                    . . ${sizetag} . .
                    <a href="${urls['']|h}">${title|h}</a> <i>(${summ})</i> ${is_redirect}
                    % if not exists:
                        </s>
                    % endif
                </li>
                <% store.continue_date = destampify(ts) %>
            % endfor
        </ul>
        <%
            continue_date = str(store.continue_date)[:-6]
            params = {
                "startdate": continue_date,
                "name": store.name,
                "ns": store.ns,
                "server": store.server,
                "max": store.max,
            }
            params = urlencode(params)
        %>
        <a href="created.py?${params|h}"><small>Next ${store.max} results &rarr;</small></a><br />
    % elif not store.error:
        No edits found
    % endif
    <br />
    <small>
        Elapsed time: ${round(time.time() - store.starttime, 3)} seconds.
        <br />
        ${arrow.now().strftime("%H:%M:%S, %d %b %Y")}
    </small>
    <br />
    <br />
    <a href="/created.py"><small>&larr;New search</small></a>
    <%include file="footer.mako" />
</body>
</html>

