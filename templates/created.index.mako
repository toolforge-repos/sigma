<!doctype html>
<%!
    import time, arrow
%>
<html>
<head>
    <%include file="header.mako" args="title='Pages created'" />
</head>
<body>
    <h1>Pages created</h1>
    <p>This tool lists pages created by a user.</p>
    <div id=indexform>
    <form action=/created.py method=GET>
        <label for=name>Username:</label>
        <input type=text id=name name=name />
        <label for=server>Database:<br/>
            <span class=gray>lang + project (<a href="//meta.wikimedia.org/w/api.php?action=sitematrix&format=jsonfm">list</a>)</span>
        </label>
        <input type=text id=server name=server value=enwiki />
        <label for=max>Max pages:<br/>
            <span class=gray>max=500</span>
        </label>
        <input type=text id=max name=max value=500 />
        <label for=startdate>Start date:<br/>
            <span class=gray>YYYYMMDD format</span>
        </label>
        <input type=text id=startdate name=startdate />
        <label for=ns>Restrict to namespaces:<br/>
            <span class=gray><var>,,</var> for mainspace</span>
        </label>
        <input type=text id=ns name=ns placeholder='Talk,Book talk,,' />
        <div>
            <label for=redirects>Redirect inclusion</label>
            <select name=redirects id=redirects
                style="width:160px;float:left;padding:2px 2px;margin:2px 0px 16px 10px;">
                <option value=coalball>All</option>
                <option value=rd>Only redirects</option>
                <option value=none>Only non-redirects</option>
            </select>
        </div>
        <div>
            <label for=deleted>Deleted creation inclusion</label>
            <select name=deleted id=deleted
                style="width:160px;float:left;padding:2px 2px;margin:2px 0px 16px 10px;">
                <option value=all>All</option>
                <option value=deleted>Only deleted</option>
                <option value=undeleted>Only non-deleted</option>
            </select>
        </div>
        <div>
            <label for=rdlink>Redirect links as noredirect</label>
            <input type=checkbox id=rdlink name=rdlink />
        </div>
        <button>Submit</button>
        <div style='clear: all'></div>
    </form>
    </div>
    <br />
    <small>
        Elapsed time: ${round(time.time() - store.starttime, 3)} seconds.
        <br />
        ${arrow.now().strftime("%H:%M:%S, %d %b %Y")}
    </small>
    <br />
    <br />
    <a href="/"><small>&larr;Home</small></a>
    <%include file="footer.mako" />
</body>
</html>

