<!doctype html>
<%
    import time, arrow, pprint, json
    from urllib.parse import urlencode
    from datetime import timedelta

    import stats

    from utils import abstract_quartiles

    today = arrow.utcnow()
    today_tuple = today.year, today.month, today.day
%>
<html>
<head>
    <%include file="header.mako" args="title='Article revision statistics'" />
    <script>
$(document).ready(function() {
        $("#editchart").scroll(function () { 
                $("#sizechart").scrollTop($("#editchart").scrollTop());
                $("#sizechart").scrollLeft($("#editchart").scrollLeft());
        });
        $("#sizechart").scroll(function () { 
                $("#editchart").scrollTop($("#sizechart").scrollTop());
                $("#editchart").scrollLeft($("#sizechart").scrollLeft());
        });

    }
);
Date.prototype.addDays = function(days) {
    var dat = new Date(this.valueOf());
    dat.setDate(dat.getDate() + days);
    return dat;
}
    </script>
    <link rel="stylesheet" type="text/css" href="/static/graphing.css" />
</head>
<body>
    <h1 id=top>Article revision statistics</h1>
    <p>This tool displays various statistics about the history of an article.</p>

    % if store.data['total_count'] > store.revlimit:
        <div class=mbox>
            Warning:
            ${store.data['total_count'] - store.data['count']}
            revisions before
            ${store.data['first_edit']['timestamp']}
            were not analysed.
        </div>
    % endif

    <hr style="text-align:left;width:875px;margin-left:0">

    <table>
        <tr>
            <td>Article:</td>
            <td>
                <a href="${store.domain|h}/wiki/${store.page|h}">${store.page|h}</a>
            </td>
        </tr>
        <tr>
            <td>Total revisions:</td>
            <td>${store.data['count']}</td>
        </tr>
        <tr>
            <td>Number of minor edits:</td>
            <td>${store.data['minor_count']} (${round(store.data['minor_count'] / (store.data['count'] or 1e99) * 100, 3)}%)</td>
        </tr>
        <tr>
            <td>Number of IP edits:</td>
            <td>${store.data['anon_count']} (${round(store.data['anon_count'] / len(store.data['editors']) * 100, 3)}%)</td>
        </tr>
        <tr>
            <td>First edit:</td>
            <td>${store.data['first_edit']['timestamp']} (by ${store.data['first_edit']['user']})</td>
        </tr>
        <tr>
            <td>Most recent edit:</td>
            <td>${store.data['last_edit']['timestamp']} (by ${store.data['last_edit']['user']})</td>
        </tr>
        <tr>
            <td>Average time between edits:</td>
            <td>${round(store.data['average_days_per_edit'], 3)} days</td>
        </tr>
        <tr>
            <td>Average number of edits per month:</td>
            <td>${round(store.data['edits_per_month'], 3)}</td>
        </tr>
        <tr>
            <td>Average number of edits per year:</td>
            <td>${round(store.data['edits_per_year'], 3)}</td>
        </tr>
        <tr>
            <td>Number of edits in the last day:</td>
            <td>${store.data['count_history'].today}</td>
        </tr>
        <tr>
            <td>Number of edits in the last week:</td>
            <td>${store.data['count_history'].week}</td>
        </tr>
        <tr>
            <td>Number of edits in the last month:</td>
            <td>${store.data['count_history'].month}</td>
        </tr>
        <tr>
            <td>Number of edits in the last year:</td>
            <td>${store.data['count_history'].year}</td>
        </tr>
        <tr>
            <td>Number of users:</td>
            <td>${store.data['editor_count']}</td>
        </tr>
        <tr>
            <td>Average edits per user:</td>
            <td>${round(store.data['edits_per_editor'], 3)}</td>
        </tr>
    </table>

    <h2 id=editsgraph>Edits over time</h2>
    <hr/>
    <div style="margin:0 auto">
        <div class="legend">
             <ul>
                 <li>
                    <form>
                        <label><input type=radio name=mode onclick="cum_edit_chart(window.cumSums, 'Total number of edits')" /> &#x222b;</label>
                        <label><input type=radio name=mode onclick="edit_chart(window.payload, 'Number of edits')" checked /> <sup>&Delta;y</sup>&frasl;<sub>&Delta;x</sub></label>
                    </form>
                 </li>
                <li><span class=dashbox style="color:crimson; font-weight:bolder;">&#9608;</span>&nbsp;&nbsp;all edits</li>
                <li><span class=dashbox style="color:mediumseagreen; font-weight:bolder;">&#9608;</span>&nbsp;&nbsp;minor edits</li>
                <li><span class=dashbox style="color:darkorchid; font-weight:bolder;">&#9608;</span>&nbsp;&nbsp;anon edits</li>
            </ul>
        </div>
        <div id=editchart style="width:60%; height:430px; padding:10px; overflow:auto; border:2px solid gray; float:left">
        </div>
    </div>

    <div style="clear:both"></div>

    <h2 id=sizegraph>Article size over time</h2>
    <hr/>
    <div style="margin:0 auto">
        <div class="legend" style='text-align:center'>
            <div style='display:inline-block'>
                <ul>
                    <li><a href="//en.wikipedia.org/wiki/Quartile"><small>(legend)</small></a></li>
                    <li>
                        <svg width=50 height=120>
                            <text x=10  y=10>Q&#x2083;</text>
                            <line x1=10 y1=20 x2=20 y2=20 class=whisker></line>
                            <line x1=15 y1=20 x2=15 y2=80 class=whisker></line>
                                <line x1=5 y1=55 x2=25 y2=45 class=line></line>
                                <circle class=focuscircle r=6 cx=15 cy=50></circle>
                                <text x=30 y=45>Q&#x2082;</text>
                            <line x1=10 y1=80 x2=20 y2=80 class=whisker></line>
                            <text x=10  y=100>Q&#x2081;</text>
                        </svg>
                    </li>
                </ul>
            </div>
        </div>
        <div id=sizechart style="width:60%; height:430px; padding:10px; overflow:auto; border:2px solid gray; float:left">
        </div>
    </div>

    <div style="clear: both"></div>
<!--
    <h2>Circle graphs even though they suck</h2>
    <hr/>
    <div style="margin:0 auto">
        <div id=circlelegend class="legend">
            &lt;3
        </div>
        <div id="circlegraphs" style="width:60%; height:430px; padding:10px; overflow:auto; border:2px solid gray; float:left">
        </div>
    </div>

    <div style="clear:both"></div>
-->
    <h2 id=top50>Top 50 contributors</h2>
    <hr/>
    <table border=1>
        <tr>
            <th>Username</th>
            <th># of edits</th>
            <th>Minor</th>
            <th>First edit</th>
            <th>Latest edit</th>
            <th>Mean time between edits</th>
            <th>Mean page size (KiB)</th>
            <th>Added (B)</th>
            <th>Removed (B)</th>
        </tr>
        % for i, (user, userobj) in enumerate(sorted(store.data['editors'].items(), key=lambda u: len(u[1].size), reverse=True)):
            <tr>
                <td>
                    <a href="${store.domain|h}/wiki/User:${user}">${user}</a>
                    (<a href="${store.domain|h}/wiki/User_talk:${user}">talk</a>)
                </td>
                <td>${len(userobj.size)}</td>
                <td>${userobj.minor} (${round(userobj.minorpct, 3)}%)</td>
                <td>${userobj.first}</td>
                <td>${userobj.last}</td>
                <td>${userobj.atbe}</td>
                <td>${round(stats.mean(userobj.size) / 1024, 3)}</td>
                <td>${userobj.addsize}</td>
                <td>${userobj.remsize}</td>
            </tr>
            % if i > 50:
<%              break %>
            % endif
        % endfor
    </table>

    <span id='warnings'>
    </span>
    <br />
    <small>
        Elapsed time: ${round(time.time() - store.starttime, 3)} seconds.
        <br />
        ${arrow.now().strftime("%H:%M:%S, %d %b %Y")}
    </small>
    <br />
    <br />
    <a href="/articleinfo.py"><small>&larr;New search</small></a>
    <%include file="footer.mako" />
    <!--<script src="/static/res/d3/3.4.8/d3.min.js"></script>-->
<script src="https://unpkg.com/d3@3.4.8/d3.min.js"></script>
<script>
//(function() {
    var margin = {top: 20, right: 50, bottom: 90, left: 50},
            width = ${(store.data['last_edit']['timestamp'] - store.data['first_edit']['timestamp']).days // 12 * 16} - margin.left - margin.right,
            height = 400 - margin.top - margin.bottom;

    if (width < 0) width *= -1;
    if (height < 0) height *= -1;

    var parseDate = d3.time.format("%Y/%m").parse,
            bisectDate = d3.bisector(function (d) {
                return d.date;
            }).left;


// ${today_tuple}
    window.payload = [
        <%
            lowest_size = 1e+99
            highest_size = 0
            prev_size = []
        %>
        % for year, yearobj in sorted(store.data['year_count'].items()):
            % for month, monthobj in enumerate(yearobj.months):
                % if monthobj is None or (year, month, 1) > today_tuple:
<%                  continue %>
                % endif
<%
                d = arrow.Arrow(year, month, 1)
                d -= timedelta(days=1)

                if not monthobj.size:
                    monthobj.size = prev_size
                prev_size = monthobj.size
                monthobj.size = [round(x / 1024, 3) for x in abstract_quartiles(monthobj.size)]
                if min(monthobj.size) < lowest_size:
                    lowest_size = min(monthobj.size)
                if max(monthobj.size) > highest_size:
                    highest_size = max(monthobj.size)
%>
                {date: new Date(${d.year}, ${d.month}, 1), crap: ${monthobj.size}},
            % endfor
        % endfor
    ];


    (function (data) {

        if (data.length < 3) {
            return $("#sizechart").text("Not enough revisions for the size chart to be worth graphing :(");
        }

        data.sort(function (a, b) {
            return a.date - b.date;
        });

        var svg = d3.select("#sizechart").append("svg")
                .attr("width", width + margin.left + margin.right)
                .attr("height", height + margin.top + margin.bottom)
                .append("g")
                .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        var x = d3.time.scale()
                .range([0, width]);

        var y = d3.scale.linear()
                .range([height, 0]);

        var xAxis = d3.svg.axis()
                .scale(x)
                .orient("bottom")
                .ticks(d3.time.months)
                .tickSize(16, 0)
                .tickFormat(d3.time.format("%Y/%m"));

        var yAxis = d3.svg.axis()
                .scale(y)
                .orient("left");

        var line = d3.svg.line()
                .x(function (d) {
                    return x(d.date);
                })
                .y(function (d) {
                    return y(d.crap[1]);
                });

        var xdomain = d3.extent(data, function (d) { return d.date} );//[data[0].date, data[data.length - 1].date];
        x.domain(xdomain);
        //var ydomain = d3.extent(data, function (d) { return d.crap[1]; });
        var ydomain = [${lowest_size - 0}, ${highest_size + 1}];  // so quartiles don't go off the screen
        y.domain(ydomain);

        svg.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(0," + height + ")")
                .call(xAxis)
                .selectAll("text")
                .attr("y", 0)
                .attr("x", 9)
                .attr("dy", ".35em")
                .style("text-anchor", "end")
                .attr("transform", "translate(0," + (margin.bottom-60) + ")" + "rotate(-45)");

        svg.append("g")
                .attr("class", "y axis")
                .call(yAxis)
                .append("text")
                .attr("transform", "rotate(-90)")
                .attr("y", 6)
                .attr("dy", ".71em")
                .style("text-anchor", "end")
                .text("Article size (KiB)");

        svg.append("path")
                .datum(data)
                .attr("class", "line")
                .attr("d", line);

        for (var i = 0; i < data.length; i++) {
            var date, dateL, dateR;
            var q1, med, q3;
            var whisker;
            date = data[i].date;
            q1 = data[i].crap[0];
            med = data[i].crap[1];
            q3 = data[i].crap[2];
            if (q1 == q3) // if there's no whisker, skip it
                continue;
            whisker = [q1, med, q3];
            svg.append("path")
                    .datum([{date: date, crap: [null, q1, null]}, {date: date, crap: [null, q3, null]}])
                    .attr("class", "whisker")
                    .attr("d", line);
            // draw the ends so it looks like a "I"
            dateL = new Date(date.getTime());
            dateR = new Date(date.getTime());
            dateL.setDate(dateL.getDate() - 3);
            dateR.setDate(dateR.getDate() + 3);
            svg.append("path")
                    .datum([{date: dateL, crap: [null, q1, null]}, {date: dateR, crap: [null, q1, null]}])
                    .attr("class", "whisker")
                    .attr("d", line);
            svg.append("path")
                    .datum([{date: dateL, crap: [null, q3, null]}, {date: dateR, crap: [null, q3, null]}])
                    .attr("class", "whisker")
                    .attr("d", line);
        }

        var focus = svg.append("g")
                .attr("class", "overlay")
                .style("display", "none");

        var circlefoc = focus.append("g");
        circlefoc.append("circle")
                .attr("class", "focuscircle")
                .attr("r", 4.5);

        circlefoc.append("text")
                .attr("x", 9)
                .attr("y", 9)
                .attr("dy", ".35em");

        focus.append("line")
                .attr("id", "lineX")
                .attr("class", "focusline");
        focus.append("line")
                .attr("id", "lineY")
                .attr("class", "focusline");

        svg.append("rect")
                .attr("class", "overlay")
                .attr("width", width)
                //.attr("fill-opacity", 0) // what the shit
                .attr("height", height)
                .on("mouseover", function () {
                    focus.style("display", null);
                })
                .on("mouseout", function () {
                    focus.style("display", "none");
                })
                .on("mousemove", mousemove);

        function mousemove() {
            var x0 = x.invert(d3.mouse(this)[0]),
                    i = bisectDate(data, x0, 1),
                    d0 = data[i - 1],
                    d1 = data[i],
                    d = x0 - d0.date > d1.date - x0 ? d1 : d0;
            circlefoc.attr("transform", "translate(" + x(d.date) + "," + y(d.crap[1]) + ")");
            circlefoc.select("text").attr("fill", "black").text(d.crap[1]);
            focus.select("#lineX")
                    .attr("x1", x(d.date)).attr("y1", y(ydomain[0]))
                    .attr("x2", x(d.date)).attr("y2", y(ydomain[1]));
            focus.select("#lineY")
                    .attr("x1", x(xdomain[0])).attr("y1", y(d.crap[1]))
                    .attr("x2", x(xdomain[1])).attr("y2", y(d.crap[1]));
        }
    })(window.payload);

    window.cumSums = [{date: new Date, crap: [0, 0, 0]}]; /* tee hee */
    window.payload = [
        % for year, yearobj in sorted(store.data['year_count'].items()):
            % for month, monthobj in enumerate(yearobj.months):
<%
                if monthobj is None or (year, month, 1) > today_tuple:
                     continue
                d = arrow.Arrow(year, month, 1)
                d -= timedelta(days=1)
%>
                {date: new Date(${d.year}, ${d.month}, 1), crap: ${[monthobj.all, monthobj.minor, monthobj.anon]}},
            % endfor
        % endfor
    ];
    var i;
    for (i = 0; i < payload.length; i++) {
        var date, next;
        date = payload[i].date;
        next = payload[i].crap.slice(0);
        for (var j = 0; j < next.length; j++)
            next[j] += cumSums[i].crap[j];
        cumSums.push({date: date, crap: next});
    }
    cumSums.splice(0, 1);

    window.cum_edit_chart = function (data, ylabel) {
        $(".dashbox").html("&mdash;");
        var chart;
        chart = $("#editchart");
        chart.empty();
        if (data.length < 3) {
            return chart.html("Not enough revisions for the edit chart to be worth graphing :(");
        }

        data.sort(function (a, b) {
            return a.date - b.date;
        });

        var svg = d3.select("#editchart").append("svg")
                .attr("width", width + margin.left + margin.right)
                .attr("height", height + margin.top + margin.bottom)
                .append("g")
                .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        var x = d3.time.scale()
                .range([0, width]);

        var y = d3.scale.linear()
                .range([height, 0]);

        var xAxis = d3.svg.axis()
                .scale(x)
                .orient("bottom")
                .ticks(d3.time.month, 1)
                .tickSize(18, 0)
                .tickFormat(d3.time.format("%Y/%m"));

        var yAxis = d3.svg.axis()
                .scale(y)
                .orient("left");

        var deltaX;
        for (var j = 0; j < 3; j++) {
            var xdomain = d3.extent(data, function (d) {
                return d.date;
            });//[data[0].date, data[data.length - 1].date];
            x.domain(xdomain);
            var ydomain = d3.extent(data, function (d) { return d.crap[0]; } ); // .crap[0] because it's the biggest
            ydomain[0] = 0; // 0 because it's the smallest
            y.domain(ydomain);

            deltaX = x(data[1].date);
            var bar_offset = deltaX / 4;

            var line = d3.svg.line()
                    .x(function (d) {
                        return x(d.date);
                    })
                    .y((function (n) {
                        return function (d) { return y(d.crap[n]); }
                    })(j));

            if (0) for (var n = 0; n < data.length; n++) {
                svg.append("rect")
                        .attr("x", x(data[n].date) + j * bar_offset)
                        .attr("width", bar_offset)
                        .attr("y", height - (height - y(data[n].crap[j])))
                        .attr("height", height - y(data[n].crap[j]))
                        .attr("class", (j == 0) ? "redblock" : (j == 1) ? "greenblock" : (j == 2) ? "purpleblock" : "");
            }
            svg.append("path")
                    .datum(data)
                    .attr("class", (j == 0) ? "redline" : (j == 1) ? "greenline" : (j == 2) ? "purpleline" : "line")
                    .attr("d", line);
            //break;
        }
        svg.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(0," + height + ")")
                .call(xAxis)
                .selectAll("text")
                .attr("y", 0)
                .attr("x", 9)
                .attr("dy", ".35em")
                .style("text-anchor", "end")
                .attr("transform", "translate(0," + (margin.bottom-60) + ")" + "rotate(-45)");

        svg.append("g")
                .attr("class", "y axis")
                .call(yAxis)
                .append("text")
                .attr("transform", "rotate(-90)")
                .attr("y", 6)
                .attr("dy", ".71em")
                .style("text-anchor", "end")
                .text(ylabel);

        var focus = svg.append("g")
                .attr("class", "overlay")
                .style("display", "none");

        var circlefoc = focus.append("g");
        circlefoc.append("circle")
                .attr("class", "focuscircle")
                .attr("r", 4.5);

        circlefoc.append("text")
                .attr("x", 9)
                .attr("y", 9)
                .attr("dy", ".35em");

        focus.append("line")
                .attr("id", "lineX")
                .attr("class", "focusline");
        focus.append("line")
                .attr("id", "lineY")
                .attr("class", "focusline");

        svg.append("rect")
                .attr("class", "overlay")
                .attr("width", width)
                //.attr("fill-opacity", 0) // what the shit
                .attr("height", height)
                .on("mouseover", function () {
                    focus.style("display", null);
                })
                .on("mouseout", function () {
                    focus.style("display", "none");
                })
                .on("mousemove", mousemove);

        function mousemove() {
            var xCoord = d3.mouse(this)[0];
            var x0 = x.invert(xCoord),
                    i = bisectDate(data, x0, 1),
                    d0 = data[i - 1],
                    d1 = data[i],
                    d = x0 - d0.date > d1.date - x0 ? d1 : d0;
            d = d0;
            var y0 = y.invert(d3.mouse(this)[1]);
            var n = 0;
            var highest = Infinity;
            var possible = d.crap.slice(0);
            for (var j = 0; j < possible.length; j++) {
                possible[j] = Math.abs(possible[j] - y0);
                if (possible[j] < highest) {
                    highest = possible[j];
                    n = j;
                }
            }
            circlefoc.attr("transform", "translate(" + (x(d.date)) + "," + y(d.crap[n]) + ")");
            circlefoc.select("text").attr("fill", "black").text(d.crap[n]);
            focus.select("#lineX")
                    .attr("x1", x(d.date)).attr("y1", y(ydomain[0]))
                    .attr("x2", x(d.date)).attr("y2", y(ydomain[1]));
            focus.select("#lineY")
                    .attr("x1", x(xdomain[0])).attr("y1", y(d.crap[n]))
                    .attr("x2", x(xdomain[1])).attr("y2", y(d.crap[n]));
        }
    };

    (window.edit_chart = function (data, ylabel) {
        $(".dashbox").html("&#9608;");
        var chart;
        chart = $("#editchart");
        chart.empty();
        if (data.length < 3) {
            return chart.html("Not enough revisions for the edit chart to be worth graphing :(");
        }

        data.sort(function (a, b) {
            return a.date - b.date;
        });

        var svg = d3.select("#editchart").append("svg")
                .attr("width", width + margin.left + margin.right)
                .attr("height", height + margin.top + margin.bottom)
                .append("g")
                .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        var x = d3.time.scale()
                .range([0, width]);

        var y = d3.scale.linear()
                .range([height, 0]);

        var xAxis = d3.svg.axis()
                .scale(x)
                .orient("bottom")
                .ticks(d3.time.month, 1)
                .tickSize(18, 0)
                .tickFormat(d3.time.format("%Y/%m"));

        var yAxis = d3.svg.axis()
                .scale(y)
                .orient("left");

        var deltaX;
        for (var j = 0; j < 3; j++) {
            var xdomain = d3.extent(data, function (d) {
                return d.date;
            });//[data[0].date, data[data.length - 1].date];
            x.domain(xdomain);
            var ydomain = d3.extent(data, function (d) { return d.crap[0]; } ); // .crap[0] because it's the biggest
            ydomain[0] = 0; // 0 because it's the smallest
            y.domain(ydomain);

            deltaX = x(data[1].date);
            var bar_offset = deltaX / 4;

            var line = d3.svg.line()
                    .x(function (d) {
                        return x(d.date);
                    })
                    .y((function (n) {
                        return function (d) { return y(d.crap[n]); }
                    })(j));

            for (var n = 0; n < data.length; n++) {
                svg.append("rect")
                        .attr("x", x(data[n].date) + j * bar_offset)
                        .attr("width", bar_offset)
                        .attr("y", height - (height - y(data[n].crap[j])))
                        .attr("height", height - y(data[n].crap[j]))
                        .attr("class", (j == 0) ? "redblock" : (j == 1) ? "greenblock" : (j == 2) ? "purpleblock" : "");
            }
            if (0) svg.append("path")
                    .datum(data)
                    .attr("class", (j == 0) ? "redline" : (j == 1) ? "greenline" : (j == 2) ? "purpleline" : "line")
                    .attr("d", line);
            //break;
        }
        svg.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(0," + height + ")")
                .call(xAxis)
                .selectAll("text")
                .attr("y", 0)
                .attr("x", 9)
                .attr("dy", ".35em")
                .style("text-anchor", "end")
                .attr("transform", "translate(0," + (margin.bottom-60) + ")" + "rotate(-45)");

        svg.append("g")
                .attr("class", "y axis")
                .call(yAxis)
                .append("text")
                .attr("transform", "rotate(-90)")
                .attr("y", 6)
                .attr("dy", ".71em")
                .style("text-anchor", "end")
                .text(ylabel);

        var focus = svg.append("g")
                .attr("class", "overlay")
                .style("display", "none");

        var circlefoc = focus.append("g");
        circlefoc.append("rect")
                .attr("class", "focusbar")
                .attr("height", 2)
                .attr("width", deltaX * 3/4);

        circlefoc.append("text")
                .attr("x", 9)
                .attr("y", -9)
                .attr("dy", ".35em");

        focus.append("line")
                .attr("id", "lineX")
                .attr("class", "focusline");
        focus.append("line")
                .attr("id", "lineY")
                .attr("class", "focusline");

        svg.append("rect")
                .attr("class", "overlay")
                .attr("width", width)
                //.attr("fill-opacity", 0) // what the shit
                .attr("height", height)
                .on("mouseover", function () {
                    focus.style("display", null);
                })
                .on("mouseout", function () {
                    focus.style("display", "none");
                })
                .on("mousemove", mousemove);

        function mousemove() {
            var xCoord = d3.mouse(this)[0];
            var x0 = x.invert(xCoord),
                    i = bisectDate(data, x0, 1),
                    d0 = data[i - 1],
                    d1 = data[i],
                    d = x0 - d0.date > d1.date - x0 ? d1 : d0;
            d = d0;
            var y0 = y.invert(d3.mouse(this)[1]);
            var n = 0;
            var highest = Infinity;
            var possible = d.crap.slice(0);
            for (var j = 0; j < possible.length; j++) {
                possible[j] = Math.abs(possible[j] - y0);
                if (possible[j] < highest) {
                    highest = possible[j];
                    n = j;
                }
            }
            circlefoc.attr("transform", "translate(" + (x(d.date)) + "," + y(d.crap[n]) + ")");
            circlefoc.select("text").attr("fill", "black").text(d.crap[n]);
            if (0) focus.select("#lineX")
                    .attr("x1", x(d.date)).attr("y1", y(ydomain[0]))
                    .attr("x2", x(d.date)).attr("y2", y(ydomain[1]));
            focus.select("#lineY")
                    .attr("x1", x(xdomain[0])).attr("y1", y(d.crap[n]))
                    .attr("x2", x(xdomain[1])).attr("y2", y(d.crap[n]));
        }
    })(payload, "Number of edits");
<%doc>
    window.circle_payload = [
            % for i, (user, userobj) in enumerate(sorted(store.data['editors'].items(), key=lambda u: u[1].addsize, reverse=True)):
                {username: ${json.dumps(user)}, addsize: ${userobj.addsize}, remsize: ${userobj.remsize}},
                % if i >= 8:
<%                  break %>
                % endif
            % endfor
    ];

    //window.circle_payload.sort(function (a, b) { return a.addsize - b.addsize });

    var n;
    var m_addsize = 0, m_remsize = 0, t_addsize = 0, t_remsize = 0;
    for (n = 0; n < window.circle_payload.length; n++)
        m_addsize += window.circle_payload[n].addsize;
    for (n = 0; n < window.circle_payload.length; n++)
        m_remsize += window.circle_payload[n].remsize;

    t_addsize = ${sum(u.addsize for u in store.data['editors'].values())};
    t_remsize = ${sum(u.remsize for u in store.data['editors'].values())};
    var rest = {username: "(other users)", addsize: t_addsize - m_remsize, remsize: t_remsize - m_remsize};
    window.circle_payload.push(rest);

    window.cirle_func = function (data, key) {
        var cwidth = 180;
        var cheight = 180;
        var radius = Math.min(cwidth, cheight) / 2;
        var colour = d3.scale.category10();
        var svg = d3.select('#circlegraphs')
                .append('svg')
                .attr('width', cwidth)
                .attr('height', cheight)
                .append('g')
                .attr('transform', 'translate(' + (cwidth / 2) + ',' + (cheight / 2) + ')');
        var arc = d3.svg.arc()
                .outerRadius(radius);
        var pie = d3.layout.pie()
                .value(key)
                .sort(null);
        var path = svg.selectAll('path')
                .data(pie(data))
                .enter()
                .append('path')
                .attr('d', arc)
                .attr('fill', function(d, i) { return colour(d.data.username) });
        var legend = $("#circlelegend");
        var names = [];
        for (n = 0; n < data.length; n++)
            names.push($("<li></li>").append($("<span></span>").css("color", colour(data[n].username)).html(data[n].username)));
        legend.empty().append("<ul></ul>").append(names);

    };
    //window.circle_func(window.circle_payload, function(d) { return d.addsize; });

    window.circle_payload = [
            % for i, (user, userobj) in enumerate(sorted(store.data['editors'].items(), key=lambda u: u[1].remsize, reverse=False)):
                {username: ${json.dumps(user)}, addsize: ${userobj.addsize}, remsize: ${userobj.remsize}},
                % if i >= 8:
<%                  break %>
                % endif
            % endfor
    ];
    window.circle_payload.push(rest);
    //window.circle_func(window.circle_payload, function(d) { return -d.remsize; });
</%doc>
//})();
</script>
</body>
</html>

