<!doctype html>
<%
    import time, arrow
    from urllib.parse import urlencode
    from utils import destampify
%>
<html>
<head>
    <%include file="header.mako" args="title='Special:Newpages queue length in days'" />

<style>
    .advanced {
        display: none;
    }
</style>
</head>
<body>
    <h1>Special:Newpages queue length in days</h1>
    <p>This tool displays the Special:Newpages queue length in days.</p>

    <hr style="text-align:left;width:875px;margin-left:0">

    <h2 id=editsgraph>Unpatrolled pages over time</h2>
    <hr/>
    <div style="margin:0 auto">
        <div id=mainchart style="width:60%; height:430px; padding:10px; overflow:auto; border:2px solid gray; float:left">
        </div>
    </div>

    <div style="clear:both"></div>


    <span id='warnings'>
        <!--${store.query.str}-->
    </span>
    <br />
    <small>
        Elapsed time: ${round(time.time() - store.starttime, 3)} seconds.
        <br />
        ${arrow.now().strftime("%H:%M:%S, %d %b %Y")}
    </small>
    <br />
    <br />
    <a href="summary.py"><small>&larr;New search</small></a>
    <%include file="footer.mako" />
    <script src="/static/res/d3/3.4.8/d3.min.js"></script>
    <script>
//(function() {
    var margin = {top: 20, right: 50, bottom: 90, left: 50},
            width = ${(store.data['last_edit']['timestamp'] - store.data['first_edit']['timestamp']).days // 12 * 16} - margin.left - margin.right,
            height = 400 - margin.top - margin.bottom;

    var parseDate = d3.time.format("%Y/%m").parse,
            bisectDate = d3.bisector(function (d) {
                return d.date;
            }).left;


    window.payload = [
        <%
            lowest_size = 1e+99
            highest_size = 0
            prev_size = 0
        %>
        % for d, size in store.data['year_count'].items():
                {date: new Date(${d.year}, ${d.month}, 1), crap: ${size}},
        % endfor
    ];


    (function (data) {

        if (data.length < 3) {
            return $("#sizechart").text("Not enough revisions for the size chart to be worth graphing :(");
        }

        data.sort(function (a, b) {
            return a.date - b.date;
        });

        var svg = d3.select("#sizechart").append("svg")
                .attr("width", width + margin.left + margin.right)
                .attr("height", height + margin.top + margin.bottom)
                .append("g")
                .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        var x = d3.time.scale()
                .range([0, width]);

        var y = d3.scale.linear()
                .range([height, 0]);

        var xAxis = d3.svg.axis()
                .scale(x)
                .orient("bottom")
                .ticks(d3.time.months)
                .tickSize(16, 0)
                .tickFormat(d3.time.format("%Y/%m"));

        var yAxis = d3.svg.axis()
                .scale(y)
                .orient("left");

        var line = d3.svg.line()
                .x(function (d) {
                    return x(d.date);
                })
                .y(function (d) {
                    return y(d.crap[1]);
                });

        var xdomain = d3.extent(data, function (d) { return d.date} );//[data[0].date, data[data.length - 1].date];
        x.domain(xdomain);
        //var ydomain = d3.extent(data, function (d) { return d.crap[1]; });
        var ydomain = [${lowest_size - 0}, ${highest_size + 1}];  // so quartiles don't go off the screen
        y.domain(ydomain);

        svg.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(0," + height + ")")
                .call(xAxis)
                .selectAll("text")
                .attr("y", 0)
                .attr("x", 9)
                .attr("dy", ".35em")
                .style("text-anchor", "end")
                .attr("transform", "translate(0," + (margin.bottom-60) + ")" + "rotate(-45)");

        svg.append("g")
                .attr("class", "y axis")
                .call(yAxis)
                .append("text")
                .attr("transform", "rotate(-90)")
                .attr("y", 6)
                .attr("dy", ".71em")
                .style("text-anchor", "end")
                .text("Article size (KiB)");

        svg.append("path")
                .datum(data)
                .attr("class", "line")
                .attr("d", line);

        for (var i = 0; i < data.length; i++) {
            var date, dateL, dateR;
            var q1, med, q3;
            var whisker;
            date = data[i].date;
            q1 = data[i].crap[0];
            med = data[i].crap[1];
            q3 = data[i].crap[2];
            if (q1 == q3) // if there's no whisker, skip it
                continue;
            whisker = [q1, med, q3];
            svg.append("path")
                    .datum([{date: date, crap: [null, q1, null]}, {date: date, crap: [null, q3, null]}])
                    .attr("class", "whisker")
                    .attr("d", line);
            // draw the ends so it looks like a "I"
            dateL = new Date(date.getTime());
            dateR = new Date(date.getTime());
            dateL.setDate(dateL.getDate() - 3);
            dateR.setDate(dateR.getDate() + 3);
            svg.append("path")
                    .datum([{date: dateL, crap: [null, q1, null]}, {date: dateR, crap: [null, q1, null]}])
                    .attr("class", "whisker")
                    .attr("d", line);
            svg.append("path")
                    .datum([{date: dateL, crap: [null, q3, null]}, {date: dateR, crap: [null, q3, null]}])
                    .attr("class", "whisker")
                    .attr("d", line);
        }

        var focus = svg.append("g")
                .attr("class", "overlay")
                .style("display", "none");

        var circlefoc = focus.append("g");
        circlefoc.append("circle")
                .attr("class", "focuscircle")
                .attr("r", 4.5);

        circlefoc.append("text")
                .attr("x", 9)
                .attr("y", 9)
                .attr("dy", ".35em");

        focus.append("line")
                .attr("id", "lineX")
                .attr("class", "focusline");
        focus.append("line")
                .attr("id", "lineY")
                .attr("class", "focusline");

        svg.append("rect")
                .attr("class", "overlay")
                .attr("width", width)
                //.attr("fill-opacity", 0) // what the shit
                .attr("height", height)
                .on("mouseover", function () {
                    focus.style("display", null);
                })
                .on("mouseout", function () {
                    focus.style("display", "none");
                })
                .on("mousemove", mousemove);

        function mousemove() {
            var x0 = x.invert(d3.mouse(this)[0]),
                    i = bisectDate(data, x0, 1),
                    d0 = data[i - 1],
                    d1 = data[i],
                    d = x0 - d0.date > d1.date - x0 ? d1 : d0;
            circlefoc.attr("transform", "translate(" + x(d.date) + "," + y(d.crap[1]) + ")");
            circlefoc.select("text").attr("fill", "black").text(d.crap[1]);
            focus.select("#lineX")
                    .attr("x1", x(d.date)).attr("y1", y(ydomain[0]))
                    .attr("x2", x(d.date)).attr("y2", y(ydomain[1]));
            focus.select("#lineY")
                    .attr("x1", x(xdomain[0])).attr("y1", y(d.crap[1]))
                    .attr("x2", x(xdomain[1])).attr("y2", y(d.crap[1]));
        }
    })(window.payload);
    </script>
</body>
</html>

