#!/home/sigma/.local/bin/python3

import json
import sys
from pathlib import *

from ceterach.api import MediaWiki
from ceterach import exceptions as exc

requested_db = None
if len(sys.argv) > 1:
    requested_db = sys.argv[1]

conf = {"defaults": {}, "retries": 3}

p = Path()/'metacrap'
mw = MediaWiki("https://meta.wikimedia.org/w/api.php", config=conf)

def do_shit(project: dict, lang: str):
    project['lang'] = lang
    dbname = project['dbname']
    try:
        ns = MediaWiki(project['url'] + "/w/api.php", config=conf).namespaces
    except exc.CeterachError as e:
        print("Skipping", dbname)
        return
    project['namespaces'] = {int(k): v for k, v in ns.items()}
    print("Updating entries for", dbname)
    with (p/(dbname+".meta")).open('w') as fp:
        json.dump(project, fp)

wikis = mw.call(action='sitematrix')['sitematrix']
for k, lang_group in wikis.items():
    if k == 'specials' or not isinstance(lang_group, dict):
        continue
    lang = lang_group['code']
    for project in lang_group['site']:
        if requested_db and project['dbname'] != requested_db:
            continue
        do_shit(project, lang)

for special_wiki in wikis['specials']:
    do_shit(special_wiki, 'en')

if requested_db == 'commonswiki' or not requested_db:
    project = {"url": "https://commons.wikimedia.org", "dbname": "commonswiki"}
    ns = MediaWiki(project['url'] + "/w/api.php").namespaces
    project['lang'] = 'en'
    project['namespaces'] = {int(k): v for k, v in ns.items()}
    dbname = project['dbname']
    print("Updating shit for Commons because they're 'special'")
    with (p/(dbname+'.meta')).open("w") as fp:
        json.dump(project, fp)
