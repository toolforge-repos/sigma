<!doctype html>
<%
    import time, arrow
%>
<html>
<head>
    <%include file="header.mako" args="title='Edit summary search'" />
</head>
<body>
    <h1>Edit summary search</h1>
    <p>
        This tool searches through a user's contribution history and returns
        the edits made by that user if the edit summary contains the specified
        string.
    </p>
    <div id='indexform'>
        <form action="/summary.py" method="GET">
            <label for='name'>Username:</label>
            <input type='text' id='name' name='name' />
            <label for='search'>Search:</label>
            <input type='text' id='search' name='search' />
            <label for='server'>
                Database:<br />
                <span class='gray'>
                    lang + project (<a href="//meta.wikimedia.org/w/api.php?action=sitematrix&format=jsonfm">list</a>)
                </span>
            </label>
            <input type='text' id='server' name='server' value='enwiki' />
            <label for='max'>
                Max pages:<br />
                <span class='gray'>max=500</span>
            </label>
            <input type='text' id='max' name='max' value='500' />
            <label for='ns'>
                Restrict to namespaces:<br />
                <span class='gray'><samp>,,</samp> for mainspace</span>
            </label>
            <input type='text' id='ns' name='ns' placeholder='Talk,Book talk,,' />
            <div>
                <!-- intentionally in the reverse order; the tool interprets "start date" in reverse chronological order -->
                <label for="enddate">
                    From date:<br />
                    <span class="gray">optional</span>
                </label>
                <input type='text' id='enddate' name='enddate' placeholder='YYYYMMDDHHMMSS' />
                <label for="startdate">
                    To date:<br />
                    <span class="gray">optional</span>
                </label>
                <input type='text' id='startdate' name='startdate' placeholder='YYYYMMDDHHMMSS' />
            </div>
            <div>
                <label for='nosect'>Don't search within /* sections */</label>
                <input type='checkbox' id='nosect' name='nosect' />
            </div>
            <div>
                <label for='casesensitive'>Case sensitive search</label>
                <input type='checkbox' id='casesensitive' name='casesensitive' />
            </div>
            <button>Submit</button>
            <div style='clear: all;'></div>
        </form>
    </div>
    <br />
    <small>
        Elapsed time: ${round(time.time() - store.starttime, 3)} seconds.
        <br />
        ${arrow.now().strftime("%H:%M:%S, %d %b %Y")}
    </small>
    <br />
    <br />
    <a href="/"><small>&larr;Home</small></a>
    <%include file="footer.mako" />
</body>
</html>

