#!/home/sigma/.local/bin/python3
# -*- coding: utf-8 -*-
# Copyright (c) 2015 User:Σ, GPLv3

import time
from urllib.parse import urlencode
from arrow import Arrow

import localeshit
from metacrap import get_namespaces, get_domain, get_lang
from utils import Databank, fmt_summ
from markupsafe import Markup

ch_locale = localeshit.ch_locale


class Tool:

    def __init__(self, params):
        self.store = store = Databank(params)
        store.query = Databank()
        store.starttime = time.time()

    def __call__(self) -> (str, Databank):
        raise NotImplementedError

    def set_constants(self):
        store = self.store

        store.server = store.server or 'enwiki'
        store.domain = get_domain(store.server)
        store.lang = get_lang(store.server)

        store.namespaces = namespaces = get_namespaces(store.server)
        store.rvnamespaces = {v.lower(): k for (k, v) in namespaces.items()}

    def titleparts(self, title):
        titleparts = ['', '', title]
        if ":" in title:
            titleparts = list(title.partition(":"))
        rvns = self.store.rvnamespaces
        for ns_name, ns_num in rvns.items():
            if titleparts[0].lower().replace(" ", "_") == ns_name.lower().replace(" ", "_"):
                titleparts[0] = ns_num
                break
        else:  # if the loop ended without being forcefully broken
            # not actually a namespace, eg [[Steins;Gate]] except not a semicolon
            titleparts = [0, '', title]
        titleparts[2] = titleparts[2].replace(" ", "_")

        return titleparts

    def maketitle(self, titleparts):
        ns, sep, title = titleparts
        ns = self.store.namespaces[ns]
        return ''.join([ns, sep, title])

    def contrib_line(self, row):
        # r is the revision, rr is the parent
        # rr.rev_parent_id,r.rev_id,r.rev_timestamp,r.rev_minor_edit,r.rev_comment,rr.rev_len,r.rev_len,page_title,page_namespace,page_latest
        #raise NotImplementedError
        parentid, revid, ts, isminor, summ, oldsize, newsize, title, namespace, topid = row
        store = self.store
        fmt = localeshit.timefmt[store.lang]
        diffword = localeshit.diff[store.lang]
        histword = localeshit.hist[store.lang]
        summ = fmt_summ(summ)
        titleparts = namespace, ":" if namespace else '', title
        page = self.maketitle(titleparts)
        top = localeshit.top[store.lang] if topid == parentid else ''
        if top:
            top = "<strong>%s</strong>" % top
        if newsize is None:
            newsize = 0
        if oldsize is None:
            oldsize = float("nan")
        if parentid is None:
            # For some reason rev_parent_id isn't populated for this row.
            # It's rumoured this is true on Wikipedia for some revisions (bug 34922)
            # Next best thing is to have the total number of bytes.
            oldsize = 0
        size = newsize - oldsize
        detail = localeshit.diffbyte[store.lang] % newsize
        ts = Arrow.strptime(str(ts), "%Y%m%d%H%M%S")
        tag = 'span' if abs(size) < 500 else 'strong'
        k = 'plusminus-null'
        if size > 0:
            k = 'plusminus-pos'
        elif size < 0:
            k = 'plusminus-neg'
        sizetag = Markup("<{0} class={1} title='{2}'>({3:+,g})</{0}>")
        sizetag = str(sizetag.format(tag, k, detail, size))
        urls = {}
        params = urlencode({"title": page, "oldid": revid})
        urls['rv'] = store.domain + "/w/index.php?" + params
        params = urlencode({"diff": "prev", "oldid": revid})
        urls['diff'] = store.domain + "/w/index.php?" + params
        params = urlencode({"title": page, "action": "history"})
        urls['hist'] = store.domain + "/w/index.php?" + params
        urls[''] = store.domain + "/wiki/" + page
        page = page.replace("_", " ")
        stamp = ch_locale(store.lang, ts.strftime, [fmt])
        minor = ''
        if isminor:
            minor = "<b>%s</b>" % localeshit.minor[store.lang] 
        return urls, ts, stamp, diffword, histword, sizetag, minor, page, summ, top

    def history_line(self, row):
        ## rr.rev_parent_id,r.rev_id,r.rev_timestamp,r.rev_minor_edit,r.rev_comment,rr.rev_len,r.rev_len,page_title,page_namespace
        # r.rev_id,r.rev_timestamp,page_namespace,page_title,r.rev_user_text,r.rev_minor_edit, rr.rev_len,r.rev_len,r.rev_comment
        revid, ts, namespace, title, user, isminor, oldsize, newsize, summ = row
        store = self.store
        # steal the actual work from contrib_line and stuff
        # fill in garbage for things we won't need... not much to do
        #       parent id   revid  ts  isminor  summ  oldsize  newsize  title  namespace  topid
        args = 0xdeadbeef, revid, ts, isminor, summ, oldsize, newsize, title, namespace, False
        urls, ts, stamp, diffword, histword, sizetag, minor, page, summ, top = self.contrib_line(args)
        urluser = user.replace(" ", "_")
        urls['u'] = store.domain + '/wiki/' + 'User:' + urluser
        urls['ut'] = store.domain + '/wiki/' + "User_talk:" + urluser
        urls['uc'] = store.domain + "/wiki/" + "Special:Contributions/" + urluser
        sizesize = "({:,g} bytes)".format(newsize)
        return urls, ts, stamp, diffword, histword, user, sizesize, sizetag, minor, page, summ
