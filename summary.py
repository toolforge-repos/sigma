#!/home/sigma/.local/bin/python3
# -*- coding: utf-8 -*-
# Copyright (c) 2015 User:Σ, GPLv3

import asyncio
import re

import aiomysql as sql

from utils import connect_info
from tool import Tool
import localeshit

ch_locale = localeshit.ch_locale


class Summary(Tool):

    def __call__(self):
        store = self.store
        if store.name is None or store.search is None:
            return "summary.index.mako", store
        self.set_constants()
        self.prepare_query()

        loop = asyncio.get_event_loop()
        loop.run_until_complete(self.do_query(store.server, store.query))

        store.query.results = self.fix_results(store.query.rawresults)
        return "summary.query.mako", store

    def set_constants(self):
        super().set_constants()
        store = self.store
        try:
            store.max = int(store.max)
            store.max = min([store.max, 500])
        except Exception:
            store.max = 500

        store.startdate = self._parse_ts(store.startdate)
        store.enddate = self._parse_ts(store.enddate)

        rvns = store.rvnamespaces
        if store.ns:
            ns_ = {rvns[x.lower()] for x in store.ns.split(",") if x.lower() in rvns}
            if ns_:
                # Only do it if there were valid namespaces
                store.ns_ = ", ".join("%s" % e for e in ns_)

    def _parse_ts(self, val):
        try:
            return int(re.sub(r'[^0-9]', '', val).ljust(14, '0'))
        except Exception:
            return 0

    def prepare_query(self):
        store = self.store
        l = []
        s = []
        if True:
            s.append(
                "SELECT rr.rev_parent_id,r.rev_id,r.rev_timestamp,r.rev_minor_edit,com.comment_text,rr.rev_len,r.rev_len,"
                "page_title,page_namespace,page_latest"
            )
            s.append("FROM revision_userindex r JOIN page ON rev_page=page_id")
            s.append("INNER JOIN comment com ON r.rev_comment_id=com.comment_id")
            s.append("LEFT JOIN revision_userindex rr ON r.rev_parent_id=rr.rev_id")
            s.append("WHERE r.rev_actor IN (SELECT actor_id FROM actor WHERE actor_name=%s)")
            l.append(store.name)
            s.append("AND r.rev_deleted=0")

            if store.casesensitive:
                s.append("AND com.comment_text LIKE %s")
                l.append(store.search.join("%%"))
            else:
                s.append("AND CONVERT(com.comment_text USING utf8mb4) LIKE %s")
                l.append(store.search.join("%%").lower())

            if store.ns_:
                s.append("AND page_namespace IN (%s)" % store.ns_)

            if store.startdate:
                s.append("AND r.rev_timestamp<=%s")
                l.append(str(store.startdate))
            if store.enddate:
                s.append("AND r.rev_timestamp>=%s")
                l.append(str(store.enddate))

            s.append("ORDER BY r.rev_timestamp DESC")
            s.append("LIMIT %s")
            l.append(store.max)
        else:
            s.append(
                "SELECT rev_parent_id,rev_id,rev_timestamp,rev_minor_edit,rev_comment,"
                "rev_parent_len,rev_len,page_title,page_namespace,page_latest"
            )
            s.append("FROM (")
            if True:
                s.append(
                    "SELECT rr.rev_parent_id,r.rev_id,r.rev_timestamp,r.rev_minor_edit,r.rev_comment,"
                    "rr.rev_len AS rev_parent_len,"
                    "r.rev_len,"
                    "page_title,page_namespace,page_latest,"
                    "r.rev_deleted"
                )
                s.append("FROM revision_userindex r")
                s.append("JOIN page ON rev_page=page_id")
                s.append("LEFT JOIN revision_userindex rr ON rr.rev_id=r.rev_parent_id")
                s.append("WHERE r.rev_actor IN (SELECT actor_id FROM actor WHERE actor_name=%s)")
                l.append(store.name)
            s.append(") AS alias")
            s.append("WHERE alias.rev_deleted=0")

            if store.casesensitive:
                s.append("AND alias.rev_comment LIKE %s")
            else:
                s.append("AND LOWER(alias.rev_comment) LIKE %s")
            l.append(store.search.join("%%").lower())

            if store.ns_:
                s.append("AND page_namespace IN (%s)" % store.ns_)

            if store.startdate:
                s.append("AND alias.rev_timestamp<=%s")
                l.append(str(store.startdate))
            if store.enddate:
                s.append("AND alias.rev_timestamp>=%s")
                l.append(str(store.enddate))

            s.append("ORDER BY alias.rev_timestamp DESC")
            s.append("LIMIT %s")
            l.append(store.max)
        store.query.str = '\n'.join(s)
        store.query.args = tuple(l)

        # prepare for the COUNT(*)
        del s[-1]
        del l[-1]
        del s[-1]
        s[0] = "SELECT COUNT(*)"
        store.query.str2 = '\n'.join(s)
        store.query.args2 = tuple(l)

    @staticmethod
    async def do_query(server, query):
        async def q(conn, qs, qa):
            cur = await conn.cursor()
            await cur.execute(qs, qa)
            return await cur.fetchall()

        async with sql.connect(**connect_info(server)) as conn1:
            async with sql.connect(**connect_info(server)) as conn2:
                query.rawresults, query.size = await asyncio.gather(q(conn1, query.str, query.args), q(conn2, query.str2, query.args2))
                query.size = query.size[0][0]

    def fix_results(self, res):
        for line in res:
            new_line = []
            for obj in line:
                if isinstance(obj, bytes):
                    obj = obj.decode("utf8")
                new_line.append(obj)
            # this next part is the worst part ever
            # parentid, revid, ts, isminor, summ, oldsize, newsize, title, namespace, topid
            yield self.contrib_line(new_line)

