<!doctype html>
<html>
<head>
    <%include file="header.mako" args="title='Error', scripts=False" />
</head>
<body>
    <h1>${status}</h1>
    <p>You shouldn't be seeing this page. Please report this to User:&Sigma; on enwp. Include the current URL and the text below in your report. <b>If you do not include this information, then there is nothing I can do to help.</b></p>
    <pre>${error}</pre>
    <%include file="footer.mako" />
</body>
</html>
