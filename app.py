#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright (c) 2015 User:Σ, GPLv3

import sys
import os
from pathlib import *

import cherrypy
from mako.lookup import TemplateLookup

cwd = Path().resolve()
tpl_lookup = TemplateLookup(directories=['templates'])#, module_directory='/data/project/sigma/')
conf = {
    '/': {
        "tools.trailing_slash.on": False,
    },
    '/static': {
        'tools.staticdir.on': True,
        'tools.staticdir.root': str(cwd),
        'tools.staticdir.dir': './static'
    }
}


def catch_errors(func):
    import functools
    import traceback

    @functools.wraps(func)
    def inner(*a, **kw):
        try:
            return func(*a, **kw)
        except Exception:
            #cherrypy.response.status = 500
            errstr = traceback.format_exc()
            #return "<pre>%s</pre>" % errstr
            return tpl_lookup.get_template('error.mako').render(status=500, error=errstr)
    return inner


class Index:

    @cherrypy.expose
    def index(self):
        return tpl_lookup.get_template("index.mako").render()

    @cherrypy.expose
    def encoding(self):
        return sys.stdout.encoding + "<br/>" + str(os.environ)

    @cherrypy.expose
    def sigma(self, *args, **kwargs):
        raise cherrypy.HTTPRedirect("/")

    @cherrypy.expose
    @catch_errors
    def created_py(self, name=None, server='enwiki', max=500, startdate=0, ns=None, redirects=None, deleted='all', rdlink=False):
        params = locals().copy()
        from created import CreatedPages
        tpl, store = CreatedPages(params)()
        tpl = tpl_lookup.get_template(tpl)
        return tpl.render(store=store)

    @cherrypy.expose
    @catch_errors
    def editorinteract_py(self, startdate=None, enddate=None, ns='', server='enwiki', allusers=False, users=None, **kw):
        # stupid crap is being stupid
        params = locals().copy()
        from editorinteract import EditorInteract
        tpl, store = EditorInteract(params)()
        tpl = tpl_lookup.get_template(tpl)
        return tpl.render(store=store)

    @cherrypy.expose
    @catch_errors
    def usersearch_py(self, name=None, page=None, server='enwiki', max=100, startdate=0, wildcards=False, casesensitive=False):
        params = locals().copy()
        from usersearch import UserSearch
        tpl, store = UserSearch(params)()
        tpl = tpl_lookup.get_template(tpl)
        return tpl.render(store=store)

    @cherrypy.expose
    @catch_errors
    def submit_py(self, image=None, video=None, desc=''):
        params = locals().copy()
        from hamp import HampHack
        tpl, store = HampHack(params)()
        tpl = tpl_lookup.get_template(tpl)
        return tpl.render(store=store)

    @cherrypy.expose
    @catch_errors
    def summary_py(self, name=None, search=None, server='enwiki', max=100, startdate=0, enddate=0, ns=None, nosect=False, casesensitive=False):
        params = locals().copy()
        from summary import Summary
        tpl, store = Summary(params)()
        tpl = tpl_lookup.get_template(tpl)
        return tpl.render(store=store)

    @cherrypy.expose
    @catch_errors
    def timeline_py(self, page=None, users=None, startdate=None, enddate=None, server='enwiki', **kw):
        # stupid crap is being stupid
        params = locals().copy()
        from timeline import Timeline
        tpl, store = Timeline(params)()
        tpl = tpl_lookup.get_template(tpl)
        return tpl.render(store=store)

    @cherrypy.expose
    @catch_errors
    def articleinfo_py(self, page=None, server='enwiki', startdate=None, enddate=None, revlimit=10000):
        params = locals().copy()
        from articleinfo import ArticleInfo
        tpl, store = ArticleInfo(params)()
        tpl = tpl_lookup.get_template(tpl)
        return tpl.render(store=store)


cherrypy.config.update({"engine.autoreload.on": False})

# Toolforge will use a variable named app
app = cherrypy.tree.mount(Index(), '/', conf)
